<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToolEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tool_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_evavaluation')->nullable();; // Relación con empresa
            $table->foreign('id_evavaluation')->references('id')->on('evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_tool')->nullable();
            $table->string('porcentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tool_evaluations');
    }
}
