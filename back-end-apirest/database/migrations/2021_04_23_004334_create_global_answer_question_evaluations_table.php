<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalAnswerQuestionEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_answer_question_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_questioneva')->nullable();; // Relación con empresa
            $table->foreign('id_questioneva')->references('id')->on('question_evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_question')->nullable();
            $table->string('name')->nullable();
            $table->string('id_type_answer')->nullable();
            $table->string('id_answer')->nullable();
            $table->string('porcentage')->nullable();
            $table->string('sub_answer')->nullable();
            $table->string('need')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_answer_question_evaluations');
    }
}
