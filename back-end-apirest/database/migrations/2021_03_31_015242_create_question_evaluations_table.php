<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_measure_evavaluation')->nullable();; // Relación con empresa
            $table->foreign('id_measure_evavaluation')->references('id')->on('measure_evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_question')->nullable();
            $table->string('porcentage')->nullable();
            $table->string('commentary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_evaluations');
    }
}
