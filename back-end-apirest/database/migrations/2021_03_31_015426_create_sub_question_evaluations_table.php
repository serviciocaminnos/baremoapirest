<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubQuestionEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_question_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_question_evaluation')->nullable();; // Relación con empresa
            $table->foreign('id_question_evaluation')->references('id')->on('question_evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_subquestion')->nullable();
            $table->string('porcentage')->nullable();
            $table->string('commentary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_question_evaluations');
    }
}
