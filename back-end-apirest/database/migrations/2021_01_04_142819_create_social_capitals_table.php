<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialCapitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_capitals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id'); // Relación con company
            $table->foreign('company_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('capital')->nullable();
            $table->string('exchange_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_capitals');
    }
}
