<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_tool_evavaluation')->nullable();; // Relación con empresa
            $table->foreign('id_tool_evavaluation')->references('id')->on('tool_evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_block')->nullable();
            $table->string('porcentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_evaluations');
    }
}
