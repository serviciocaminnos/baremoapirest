<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('questions_id');
            $table->foreign('questions_id')->references('id')->on('questions')->onDelete('cascade'); // clave foranea
            $table->unsignedBigInteger('answer_type_id')->nullable();
            $table->foreign('answer_type_id')->references('id')->on('answer_types'); // clave foranea
            $table->string('name', 2000);
            $table->string('description', 2000)->nullable();
            $table->integer('position');
            $table->integer('porcentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_questions');
    }
}
