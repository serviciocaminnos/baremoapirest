<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowerOfTheGoverningBodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_of_the_governing_bodies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id')->nullable(); // Relación con empresa
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('ordinary')->nullable();
            $table->string('extraordinary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_of_the_governing_bodies');
    }
}
