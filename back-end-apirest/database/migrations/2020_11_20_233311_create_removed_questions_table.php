<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemovedQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('removed_questions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('removed_measure_id'); // Relación
            $table->bigInteger('removed_answer_type_id')->nullable();
            $table->string('name', 2000);
            $table->string('description', 2000);
            $table->integer('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('removed_questions');
    }
}
