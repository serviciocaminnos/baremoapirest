<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepsTransferOfFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps_transfer_of_fees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id')->nullable(); // Relación con empresa
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('step_name')->nullable();
            $table->string('description')->nullable();
            $table->string('time_limit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steps_transfer_of_fees');
    }
}
