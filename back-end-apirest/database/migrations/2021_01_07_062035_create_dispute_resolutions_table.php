<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisputeResolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispute_resolutions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id'); // Relación con Nomina de soscios
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('instance')->nullable();
            $table->string('metod')->nullable();
            $table->string('regulation')->nullable();
            $table->string('type')->nullable();
            $table->string('campus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispute_resolutions');
    }
}
