<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtraCompanyDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_company_datas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id'); // Relación con Nomina de soscios
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('data_name')->nullable();
            $table->string('data')->nullable();
            $table->string('type')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_company_datas');
    }
}
