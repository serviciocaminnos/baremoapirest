<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecificLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specific_limits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('representative_id')->nullable(); // Relación con empresa
            $table->foreign('representative_id')->references('id')->on('representatives')->onDelete('cascade'); // clave foranea
            $table->string('type')->nullable();
            $table->string('amount')->nullable();
            $table->string('compentent_body')->nullable();
            $table->string('mechanism')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specific_limits');
    }
}
