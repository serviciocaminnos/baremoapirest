<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapitalContributionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capital_contribution_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('social_capitals_id'); // Relación con socios
            $table->foreign('social_capitals_id')->references('id')->on('social_capitals')->onDelete('cascade'); // clave foranea
            $table->string('cash')->nullable();
            $table->string('equipment')->nullable();
            $table->string('moviliario')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capital_contribution_types');
    }
}
