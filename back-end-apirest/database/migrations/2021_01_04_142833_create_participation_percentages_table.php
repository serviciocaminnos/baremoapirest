<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipationPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participation_percentages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('partners_id'); // Relación con socios
            $table->foreign('partners_id')->references('id')->on('partners')->onDelete('cascade'); // clave foranea
            $table->unsignedBigInteger('social_capitals_id'); // Relación con socios
            $table->foreign('social_capitals_id')->references('id')->on('social_capitals')->onDelete('cascade'); // clave foranea
            $table->string('input')->nullable();
            $table->string('capital_quota')->nullable();
            $table->string('participation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations. 
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participation_percentages');
    }
}
