<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemovedBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('removed_blocks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('removed_tool_id'); // Relación con herramientas
            $table->string('name', 1000);
            $table->double('porcentage');
            $table->string('description', 1000);
            $table->integer('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('removed_blocks');
    }
}
