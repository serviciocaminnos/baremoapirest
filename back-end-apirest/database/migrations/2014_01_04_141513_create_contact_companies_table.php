<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_companies', function (Blueprint $table) {
            $table->id();
            $table->string('kind_of_society')->nullable();
            $table->string('business_name')->nullable();
            $table->string('trade_registration')->nullable();
            $table->integer('id_tool')->nullable();
            $table->string('nit')->nullable();
            $table->string('type_of_item')->nullable();
            $table->string('date')->nullable();
            $table->string('legal_address')->nullable();
            $table->string('direction')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('url_logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_companies');
    }
}
