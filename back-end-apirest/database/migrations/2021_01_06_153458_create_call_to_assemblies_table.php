<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallToAssembliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_to_assemblies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id')->nullable(); // Relación con empresa
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('medium')->nullable();
            $table->string('anticipation_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_to_assemblies');
    }
}
