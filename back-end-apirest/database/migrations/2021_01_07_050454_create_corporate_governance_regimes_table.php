<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateGovernanceRegimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_governance_regimes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_legal_reserves')->nullable(); // Relación con empresa
            $table->foreign('id_legal_reserves')->references('id')->on('legal_reserves')->onDelete('cascade'); // clave foranea
            $table->string('ordinary')->nullable();
            $table->string('extraordinary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporate_governance_regimes');
    }
}
