<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasureEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_evaluations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_block_evavaluation')->nullable();; // Relación con empresa
            $table->foreign('id_block_evavaluation')->references('id')->on('block_evaluations')->onDelete('cascade'); // clave foranea
            $table->string('id_measure')->nullable();
            $table->string('porcentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_evaluations');
    }
}
