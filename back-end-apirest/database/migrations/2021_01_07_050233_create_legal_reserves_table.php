<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegalReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_reserves', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('participation_id')->nullable(); // Relación con empresa
            $table->foreign('participation_id')->references('id')->on('participation_percentages')->onDelete('cascade'); // clave foranea
            $table->string('legal_reserve_fund')->nullable();
            $table->string('annual_percentage_retained')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_reserves');
    }
}
