<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemovedMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('removed_measures', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('removed_block_id'); // Relación con herramientas
            $table->string('name', 1000);
            $table->string('description', 1000);
            $table->integer('position');
            $table->double('porcentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('removed_measures');
    }
}
