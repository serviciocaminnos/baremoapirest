<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_id'); // Relación con Nomina de soscios
            $table->foreign('contact_id')->references('id')->on('contact_companies')->onDelete('cascade'); // clave foranea
            $table->string('name')->nullable();          
            $table->string('identification_document')->nullable();
            $table->string('issued_in')->nullable();
            $table->string('attorney')->nullable();
            $table->string('instrument_power')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
