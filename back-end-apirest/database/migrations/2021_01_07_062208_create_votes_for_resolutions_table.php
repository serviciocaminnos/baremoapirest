<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesForResolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes_for_resolutions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('corporate_governance_regimes_id')->nullable(); // Relación con empresa
            $table->foreign('corporate_governance_regimes_id')->references('id')->on('corporate_governance_regimes')->onDelete('cascade'); // clave foranea
            $table->string('ordinary')->nullable();
            $table->string('extraordinary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes_for_resolutions');
    }
}
