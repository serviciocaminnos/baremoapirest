-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-04-2021 a las 06:03:56
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdevaluador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `answer_type_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `porcentage` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`id`, `answer_type_id`, `name`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(4, 2, 'Answer1', 1, 30, '2020-12-05 07:30:12', '2020-12-05 07:30:12'),
(5, 2, 'Answer1', 2, 30, '2020-12-05 07:30:13', '2020-12-05 07:30:13'),
(6, 2, 'Answer1', 3, 30, '2020-12-05 07:30:15', '2020-12-05 07:30:15'),
(7, 2, 'Answer7232323223', 4, 30, '2020-12-05 07:30:16', '2021-01-15 02:49:47'),
(8, 2, 'Answer1', 5, 30, '2020-12-05 07:30:17', '2020-12-05 07:30:17'),
(9, 2, 'Answer1', 6, 30, '2020-12-05 07:30:21', '2020-12-05 07:30:21'),
(10, 2, 'Answer1', 7, 30, '2020-12-05 07:30:25', '2020-12-05 07:30:25'),
(11, 3, 'Answer1', 1, 30, '2020-12-05 07:30:33', '2020-12-05 07:30:33'),
(12, 3, 'Answer1', 1, 30, '2020-12-05 07:30:34', '2020-12-05 07:30:34'),
(13, 3, 'Answer1', 2, 30, '2020-12-05 07:30:35', '2020-12-05 07:30:35'),
(14, 3, 'Answer1', 3, 30, '2020-12-05 07:31:22', '2020-12-05 07:31:22'),
(15, 3, 'Answer1', 5, 30, '2021-01-15 02:49:39', '2021-01-15 02:49:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer_types`
--

CREATE TABLE `answer_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `answer_types`
--

INSERT INTO `answer_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Answer type1', '2020-12-05 07:19:55', '2020-12-05 07:19:55'),
(3, 'Answer type1', '2020-12-05 07:19:57', '2020-12-05 07:19:57'),
(4, 'Answer type1', '2020-12-05 07:19:58', '2020-12-05 07:19:58'),
(5, 'Answer type5', '2020-12-05 07:19:59', '2021-01-15 02:44:20'),
(6, 'Answer type1', '2020-12-05 07:20:00', '2020-12-05 07:20:00'),
(7, 'Answer type1', '2020-12-05 07:20:03', '2020-12-05 07:20:03'),
(8, 'Answer type1', '2020-12-05 07:20:06', '2020-12-05 07:20:06'),
(9, 'Answer type1', '2020-12-05 07:20:08', '2020-12-05 07:20:08'),
(10, 'Answer type1', '2020-12-05 07:20:09', '2020-12-05 07:20:09'),
(11, 'Answer type1', '2021-01-15 02:44:16', '2021-01-15 02:44:16'),
(12, 'Answer type1', '2021-01-15 02:44:16', '2021-01-15 02:44:16'),
(13, 'Answer type1', '2021-01-15 02:44:17', '2021-01-15 02:44:17'),
(14, 'Answer type1', '2021-01-15 02:50:13', '2021-01-15 02:50:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blocks`
--

CREATE TABLE `blocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tool_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `porcentage` double NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blocks`
--

INSERT INTO `blocks` (`id`, `tool_id`, `name`, `porcentage`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, 2, 'block', 40, 'block', 1, '2020-12-05 07:12:06', '2021-04-27 02:33:29'),
(2, 1, 'block11', 30, 'block1', 1, '2020-12-05 07:12:14', '2021-04-24 06:37:55'),
(3, 3, 'block1', 30, 'block1', 3, '2020-12-05 07:12:18', '2020-12-05 07:12:18'),
(4, 4, 'block1', 30, 'block1', 4, '2020-12-05 07:12:22', '2020-12-05 07:12:22'),
(5, 1, 'block1', 30, 'block1', 2, '2020-12-05 07:12:26', '2020-12-06 08:09:12'),
(8, 1, '23e23e23', 30, 'block1', 4, '2020-12-05 07:12:30', '2021-01-15 02:11:02'),
(9, 1, 'block1', 30, 'block1', 5, '2020-12-05 07:12:32', '2020-12-08 20:22:17'),
(10, 1, 'block1', 30, 'block1', 6, '2020-12-05 07:12:33', '2020-12-08 20:22:17'),
(11, 1, 'block11', 30, 'block1', 3, '2020-12-08 20:16:16', '2021-01-14 23:54:27'),
(12, 1, 'block1', 30, 'block1', 7, '2020-12-08 20:18:44', '2020-12-08 20:22:17'),
(13, 1, 'block1', 30, 'block1', 8, '2020-12-08 20:20:14', '2020-12-08 20:22:17'),
(14, 1, 'block1', 30, 'block1', 9, '2020-12-08 20:22:34', '2020-12-08 20:22:34'),
(15, 2, 'block1', 30, 'block1', 2, '2020-12-08 20:24:46', '2020-12-08 20:24:46'),
(16, 2, 'block1', 30, 'block1', 3, '2021-01-15 02:09:54', '2021-01-15 02:09:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `block_evaluations`
--

CREATE TABLE `block_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_tool_evavaluation` bigint(20) UNSIGNED DEFAULT NULL,
  `id_block` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `block_evaluations`
--

INSERT INTO `block_evaluations` (`id`, `id_tool_evavaluation`, `id_block`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '20%', '2021-04-08 11:25:36', '2021-04-08 11:25:36'),
(2, 1, '2', '20%', '2021-04-08 11:25:51', '2021-04-08 11:25:51'),
(3, 1, '3', '20%', '2021-04-08 11:26:07', '2021-04-08 11:26:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `call_to_assemblies`
--

CREATE TABLE `call_to_assemblies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `medium` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anticipation_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `call_to_assemblies`
--

INSERT INTO `call_to_assemblies` (`id`, `contact_id`, `medium`, `anticipation_period`, `created_at`, `updated_at`) VALUES
(1, 1, '2342213321213213213', '12213', '2021-01-14 08:21:50', '2021-01-15 02:53:13'),
(2, 1, '2342', '12213', '2021-01-14 08:21:51', '2021-01-14 08:21:51'),
(3, 1, '2342', '12213', '2021-01-14 08:21:51', '2021-01-14 08:21:51'),
(4, 1, '2342', '12213', '2021-01-14 08:21:52', '2021-01-14 08:21:52'),
(5, 1, '2342', '12213', '2021-01-15 02:53:03', '2021-01-15 02:53:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capital_contribution_types`
--

CREATE TABLE `capital_contribution_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `social_capitals_id` bigint(20) UNSIGNED NOT NULL,
  `cash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `moviliario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `capital_contribution_types`
--

INSERT INTO `capital_contribution_types` (`id`, `social_capitals_id`, `cash`, `equipment`, `moviliario`, `created_at`, `updated_at`) VALUES
(1, 1, '1200', '2132', '321231213', '2021-01-14 08:21:39', '2021-01-14 08:21:39'),
(2, 1, '1200', '2132', '321231213', '2021-01-14 08:21:40', '2021-01-14 08:21:40'),
(3, 1, '1200', '2132', '321231213', '2021-01-14 08:21:41', '2021-01-14 08:21:41'),
(4, 1, '1200', '2132', '321231213', '2021-01-14 08:21:42', '2021-01-14 08:21:42'),
(5, 1, '1200', '2132', '321231213', '2021-01-14 08:21:42', '2021-01-14 08:21:42'),
(6, 1, '1200', '2132', '321231213', '2021-01-14 08:21:43', '2021-01-14 08:21:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact_companies`
--

CREATE TABLE `contact_companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kind_of_society` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trade_registration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_tool` int(11) DEFAULT NULL,
  `nit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_item` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legal_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contact_companies`
--

INSERT INTO `contact_companies` (`id`, `kind_of_society`, `business_name`, `trade_registration`, `id_tool`, `nit`, `type_of_item`, `date`, `legal_address`, `direction`, `contact_person`, `contact_number`, `contact_email`, `url_logo`, `created_at`, `updated_at`) VALUES
(1, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', NULL, '2021-01-07 12:12:25', '2021-01-07 12:12:25'),
(2, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', NULL, '2021-01-07 12:12:47', '2021-01-07 12:12:47'),
(3, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', NULL, '2021-01-15 02:51:33', '2021-01-15 02:51:33'),
(4, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', NULL, '2021-04-12 08:33:11', '2021-04-12 08:33:11'),
(5, 'weqwqe', 'ewwqe', 'qwqew', 1, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', NULL, '2021-04-27 09:30:59', '2021-04-27 09:30:59'),
(6, 'weqwqe', 'ewwqe', 'qwqew', 1, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', 'hola', '2021-04-27 09:31:19', '2021-04-27 09:31:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `corporate_governance_regimes`
--

CREATE TABLE `corporate_governance_regimes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_legal_reserves` bigint(20) UNSIGNED DEFAULT NULL,
  `ordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extraordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `corporate_governance_regimes`
--

INSERT INTO `corporate_governance_regimes` (`id`, `id_legal_reserves`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 1, 'saassasa', 'qwdqw', '2021-01-14 08:22:18', '2021-01-14 08:22:18'),
(2, 1, 'saassasa', 'qwdqw', '2021-01-14 08:22:19', '2021-01-15 03:01:00'),
(3, 1, 'saassasa', 'qwdqw', '2021-01-14 08:22:19', '2021-01-14 08:22:19'),
(4, 1, 'saassasa', 'qwdqw', '2021-01-15 02:58:13', '2021-01-15 02:58:13'),
(5, 1, 'saassasa', 'qwdqw', '2021-01-15 02:58:15', '2021-01-15 02:58:15'),
(6, 1, 'saassasa', 'qwdqw', '2021-01-15 02:58:15', '2021-01-15 02:58:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispute_resolutions`
--

CREATE TABLE `dispute_resolutions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metod` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `regulation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `campus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `dispute_resolutions`
--

INSERT INTO `dispute_resolutions` (`id`, `contact_id`, `instance`, `metod`, `regulation`, `type`, `campus`, `created_at`, `updated_at`) VALUES
(1, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 08:22:25', '2021-01-14 08:22:25'),
(2, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 08:22:26', '2021-01-14 08:22:26'),
(3, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 08:22:26', '2021-01-14 08:22:26'),
(4, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 08:22:27', '2021-01-14 08:22:27'),
(5, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 08:22:27', '2021-01-14 08:22:27'),
(6, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-15 03:02:17', '2021-01-15 03:02:17'),
(7, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-15 03:02:20', '2021-01-15 03:02:20'),
(8, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-15 03:02:21', '2021-01-15 03:02:21'),
(9, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-15 03:02:22', '2021-01-15 03:02:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluations`
--

CREATE TABLE `evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `evaluations`
--

INSERT INTO `evaluations` (`id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-04-08 11:24:46', '2021-04-08 11:24:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extra_company_datas`
--

CREATE TABLE `extra_company_datas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `data_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `extra_company_datas`
--

INSERT INTO `extra_company_datas` (`id`, `contact_id`, `data_name`, `data`, `type`, `url`, `created_at`, `updated_at`) VALUES
(3, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 16:02:00', '2021-04-02 16:02:00'),
(4, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 16:02:01', '2021-04-02 16:02:01'),
(5, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 16:02:01', '2021-04-02 16:02:01'),
(6, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 16:02:02', '2021-04-02 16:02:02'),
(7, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 10:17:04', '2021-04-12 10:17:04'),
(8, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 10:28:34', '2021-04-12 10:28:34'),
(9, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 10:33:05', '2021-04-12 10:33:05'),
(10, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 10:47:54', '2021-04-12 10:47:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `global_answer_questions`
--

CREATE TABLE `global_answer_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_question_conf` bigint(20) UNSIGNED DEFAULT NULL,
  `id_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `need` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `global_answer_questions`
--

INSERT INTO `global_answer_questions` (`id`, `id_question_conf`, `id_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, '2', 'sdfsd', '1', '2', '10', 'dfg', 'iudf', '2021-04-24 02:02:39', '2021-04-28 02:38:16'),
(2, 1, '2', 'sdfsd', '1', '2', '10', 'dfg', 'iudf', '2021-04-24 02:02:42', '2021-04-28 02:08:00'),
(3, 1, '2', 'sdfsd', '1', '2', '7', 'dfg', 'iudf', '2021-04-24 02:02:42', '2021-04-28 02:40:50'),
(4, 1, '2', 'sdfsd', '1', '2', '3', 'dfg', 'iudf', '2021-04-27 22:59:07', '2021-04-28 02:41:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `global_answer_question_evaluations`
--

CREATE TABLE `global_answer_question_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_questioneva` bigint(20) UNSIGNED DEFAULT NULL,
  `id_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `need` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `global_answer_question_evaluations`
--

INSERT INTO `global_answer_question_evaluations` (`id`, `id_questioneva`, `id_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:12', '2021-04-28 02:48:12'),
(2, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:13', '2021-04-28 02:48:13'),
(3, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:14', '2021-04-28 02:48:14'),
(4, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:15', '2021-04-28 02:48:15'),
(5, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:16', '2021-04-28 02:48:16'),
(6, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:16', '2021-04-28 02:48:16'),
(7, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-28 02:48:17', '2021-04-28 02:48:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `global_answer_sub_questions`
--

CREATE TABLE `global_answer_sub_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_sub_question_conf` bigint(20) UNSIGNED DEFAULT NULL,
  `id_sub_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `need` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `global_answer_sub_questions`
--

INSERT INTO `global_answer_sub_questions` (`id`, `id_sub_question_conf`, `id_sub_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 3, '2', 'answer data update', '1', '2', '5', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 02:03:20', '2021-04-28 03:54:04'),
(2, 3, '2', 'answer data update', '1', '2', '5', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 02:03:21', '2021-04-28 03:54:28'),
(3, 3, '2', 'answer data update', '1', '2', '5', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 02:03:21', '2021-04-28 03:54:32'),
(4, 3, '2', 'answer data update', '1', '2', '2.5', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 02:03:22', '2021-04-28 03:54:58'),
(5, 3, '2', 'answer data update', '1', '2', '2.5', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 02:03:22', '2021-04-28 03:55:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `global_answer_sub_question_evaluations`
--

CREATE TABLE `global_answer_sub_question_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_subquestioneva` bigint(20) UNSIGNED DEFAULT NULL,
  `id_sub_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `need` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `global_answer_sub_question_evaluations`
--

INSERT INTO `global_answer_sub_question_evaluations` (`id`, `id_subquestioneva`, `id_sub_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 01:59:20', '2021-04-24 01:59:20'),
(2, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 01:59:56', '2021-04-24 01:59:56'),
(3, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 01:59:57', '2021-04-24 01:59:57'),
(4, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 01:59:57', '2021-04-24 01:59:57'),
(5, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-24 01:59:58', '2021-04-24 01:59:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `legal_reserves`
--

CREATE TABLE `legal_reserves` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `participation_id` bigint(20) UNSIGNED DEFAULT NULL,
  `legal_reserve_fund` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_percentage_retained` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `legal_reserves`
--

INSERT INTO `legal_reserves` (`id`, `participation_id`, `legal_reserve_fund`, `annual_percentage_retained`, `created_at`, `updated_at`) VALUES
(1, 1, '123132321231213', '4342234', '2021-01-14 08:21:58', '2021-01-15 02:58:03'),
(2, 1, '322332', '4342234', '2021-01-14 08:21:59', '2021-01-14 08:21:59'),
(3, 1, '322332', '4342234', '2021-01-14 08:21:59', '2021-01-14 08:21:59'),
(4, 1, '322332', '4342234', '2021-01-14 08:22:00', '2021-01-14 08:22:00'),
(5, 1, '322332', '4342234', '2021-01-15 02:57:05', '2021-01-15 02:57:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measures`
--

CREATE TABLE `measures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `block_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `porcentage` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `measures`
--

INSERT INTO `measures` (`id`, `block_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, 'measure2oqwndoiqmwoimwq', 'measure2cwqqwcqwcwqwqcwcqwcq', 1, 10, '2020-12-05 07:14:55', '2021-04-25 03:57:04'),
(2, 1, 'measure2', 'measure2', 1, 30, '2020-12-05 07:15:03', '2020-12-08 20:26:10'),
(3, 3, 'measure2', 'measure2', 3, 30, '2020-12-05 07:15:07', '2020-12-05 07:15:07'),
(4, 4, 'measure2', 'measure2', 4, 30, '2020-12-05 07:15:11', '2020-12-05 07:15:11'),
(5, 5, 'measure2', 'measure2', 5, 30, '2020-12-05 07:15:16', '2020-12-05 07:15:16'),
(6, 1, 'measure6', 'measure6', 2, 30, '2020-12-05 07:15:21', '2020-12-08 20:26:10'),
(7, 1, 'measure2', 'measure2', 3, 30, '2020-12-05 07:15:22', '2020-12-08 20:26:10'),
(8, 1, 'measure2', 'measure2', 5, 30, '2020-12-05 07:15:24', '2020-12-08 20:26:10'),
(10, 1, 'measure2', 'measure2', 6, 30, '2020-12-05 07:15:26', '2020-12-08 20:26:10'),
(11, 1, 'measure2', 'measure2', 4, 30, '2020-12-08 20:25:39', '2020-12-08 20:26:10'),
(12, 1, 'measure2', 'measure2', 7, 30, '2020-12-08 20:26:50', '2020-12-08 20:26:50'),
(13, 1, 'measure2', 'measure2', 8, 30, '2020-12-08 20:26:52', '2020-12-08 20:26:52'),
(14, 2, 'measure', 'measure', 2, 15, '2020-12-08 20:27:23', '2021-04-27 23:52:07'),
(15, 2, 'measure1', 'measure1', 3, 15, '2021-01-15 02:33:21', '2021-04-27 23:52:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measure_evaluations`
--

CREATE TABLE `measure_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_block_evavaluation` bigint(20) UNSIGNED DEFAULT NULL,
  `id_measure` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `measure_evaluations`
--

INSERT INTO `measure_evaluations` (`id`, `id_block_evavaluation`, `id_measure`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 3, '1', '20%', '2021-04-08 11:26:48', '2021-04-08 11:26:48'),
(2, 3, '2', '20%', '2021-04-08 11:27:33', '2021-04-08 11:27:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_04_141513_create_contact_companies_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2020_11_01_100119_create_tools_table', 1),
(6, '2020_11_02_142521_create_blocks_table', 1),
(7, '2020_11_09_154232_create_measures_table', 1),
(8, '2020_11_09_154240_create_answer_types_table', 1),
(9, '2020_11_09_154541_create_questions_table', 1),
(10, '2020_11_09_154542_create_sub_questions_table', 1),
(11, '2020_11_10_042051_create_answers_table', 1),
(12, '2020_11_20_233215_create_removed_tools_table', 1),
(13, '2020_11_20_233230_create_removed_blocks_table', 1),
(14, '2020_11_20_233256_create_removed_measures_table', 1),
(15, '2020_11_20_233311_create_removed_questions_table', 1),
(16, '2020_11_20_233333_create_removed_answer_types_table', 1),
(17, '2020_12_03_221019_create_removed_answers_table', 1),
(18, '2021_01_04_142249_create_partners_table', 1),
(19, '2021_01_04_142819_create_social_capitals_table', 1),
(20, '2021_01_04_142833_create_participation_percentages_table', 1),
(21, '2021_01_04_143035_create_capital_contribution_types_table', 1),
(22, '2021_01_06_150237_create_representatives_table', 1),
(23, '2021_01_06_153022_create_specific_limits_table', 1),
(24, '2021_01_06_153458_create_call_to_assemblies_table', 1),
(25, '2021_01_06_153658_create_steps_transfer_of_fees_table', 1),
(26, '2021_01_07_031009_create_power_of_the_governing_bodies_table', 1),
(27, '2021_01_07_050233_create_legal_reserves_table', 1),
(28, '2021_01_07_050454_create_corporate_governance_regimes_table', 1),
(29, '2021_01_07_062035_create_dispute_resolutions_table', 1),
(30, '2021_01_07_062208_create_votes_for_resolutions_table', 1),
(31, '2021_01_12_195619_create_extra_company_datas_table', 1),
(32, '2021_03_31_014827_create_evaluations_table', 1),
(33, '2021_03_31_014944_create_tool_evaluations_table', 1),
(34, '2021_03_31_015038_create_block_evaluations_table', 1),
(35, '2021_03_31_015043_create_measure_evaluations_table', 1),
(36, '2021_03_31_015242_create_question_evaluations_table', 1),
(37, '2021_03_31_015426_create_sub_question_evaluations_table', 1),
(38, '2021_04_12_144234_create_global_answer_sub_questions_table', 1),
(39, '2021_04_12_144747_create_global_answer_questions_table', 1),
(40, '2021_04_13_163326_create_user_permits_table', 1),
(41, '2021_04_23_004334_create_global_answer_question_evaluations_table', 1),
(42, '2021_04_23_004502_create_global_answer_sub_question_evaluations_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participation_percentages`
--

CREATE TABLE `participation_percentages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `partners_id` bigint(20) UNSIGNED NOT NULL,
  `social_capitals_id` bigint(20) UNSIGNED NOT NULL,
  `input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capital_quota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `participation_percentages`
--

INSERT INTO `participation_percentages` (`id`, `partners_id`, `social_capitals_id`, `input`, `capital_quota`, `participation`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 08:21:23', '2021-01-14 08:21:23'),
(2, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 08:21:26', '2021-01-14 08:21:26'),
(3, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 08:21:27', '2021-01-14 08:21:27'),
(4, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 08:21:29', '2021-01-14 08:21:29'),
(5, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-15 02:52:33', '2021-01-15 02:52:33'),
(6, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-15 02:52:47', '2021-01-15 02:52:47'),
(7, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-15 02:52:47', '2021-01-15 02:52:47'),
(8, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-15 02:52:48', '2021-01-15 02:52:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identification_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issued_in` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attorney` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instrument_power` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `partners`
--

INSERT INTO `partners` (`id`, `contact_id`, `name`, `identification_document`, `issued_in`, `attorney`, `instrument_power`, `created_at`, `updated_at`) VALUES
(1, 1, 'adwqwqddwq', '1231231', NULL, 'qwwqwqwq', NULL, '2021-01-07 12:36:53', '2021-01-07 12:41:34'),
(2, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', NULL, 'qwwqwqwq', NULL, '2021-01-07 12:37:25', '2021-01-07 12:37:25'),
(3, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', NULL, 'qwwqwqwq', NULL, '2021-01-07 12:37:26', '2021-01-07 12:37:26'),
(4, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', 'wqdwqdqwdwqd', 'wqdwqdqwdwqd', 'qwwqwqwq', '2021-01-15 02:52:13', '2021-01-15 02:52:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'authToken', 'd0c8bf36ba52cfa0ff70f3dfec8e481578ff4f2fc732fcd76151d637cd317e85', '[\"*\"]', '2021-04-28 07:59:03', '2020-12-05 03:08:07', '2021-04-28 07:59:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `power_of_the_governing_bodies`
--

CREATE TABLE `power_of_the_governing_bodies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extraordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `power_of_the_governing_bodies`
--

INSERT INTO `power_of_the_governing_bodies` (`id`, `contact_id`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 1, '213423', 'dqwqwdwdqwwqd', '2021-01-14 08:22:33', '2021-01-15 03:05:07'),
(2, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 08:22:34', '2021-01-14 08:22:34'),
(3, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 08:22:35', '2021-01-14 08:22:35'),
(4, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 08:22:36', '2021-01-14 08:22:36'),
(5, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 08:22:37', '2021-01-14 08:22:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `measure_id` bigint(20) UNSIGNED NOT NULL,
  `answer_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `porcentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`id`, `measure_id`, `answer_type_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 4, NULL, '1qew1231231231231231', 'description', 1, NULL, '2020-12-05 07:21:18', '2021-01-15 02:39:30'),
(2, 4, 2, 'question', 'description', 2, 30, '2020-12-05 07:21:21', '2021-04-28 02:07:49'),
(3, 1, NULL, '1qewq', '234', 3, NULL, '2020-12-05 07:21:25', '2021-01-15 00:25:16'),
(4, 1, NULL, 'question1', 'description', 4, NULL, '2020-12-05 07:21:26', '2020-12-06 08:14:54'),
(5, 1, NULL, 'question1', 'description', 5, NULL, '2020-12-05 07:21:27', '2020-12-06 08:14:55'),
(6, 1, NULL, 'question1', 'description', 6, NULL, '2020-12-05 07:21:28', '2020-12-06 08:14:55'),
(7, 1, NULL, 'question1', 'description', 7, NULL, '2020-12-05 07:21:30', '2020-12-06 08:14:55'),
(8, 1, NULL, 'question1', 'description', 8, NULL, '2020-12-05 07:21:31', '2020-12-06 08:14:55'),
(9, 3, NULL, 'question', 'description', 9, 10, '2020-12-05 07:21:35', '2021-04-27 23:56:37'),
(10, 3, NULL, 'question', 'description', 10, 10, '2020-12-05 07:21:36', '2021-04-27 23:59:37'),
(11, 3, NULL, 'question', 'description', 11, 5, '2020-12-05 07:21:37', '2021-04-28 00:02:33'),
(12, 3, NULL, 'question', 'description', 12, 5, '2020-12-05 07:21:38', '2021-04-28 00:02:35'),
(13, 2, NULL, 'question1', 'description', 13, NULL, '2020-12-05 07:21:43', '2020-12-06 08:14:55'),
(14, 2, NULL, 'question1', 'description', 14, NULL, '2020-12-05 07:21:44', '2020-12-06 08:14:55'),
(15, 2, NULL, 'question1', 'description', 3, NULL, '2021-01-14 23:58:20', '2021-01-14 23:58:20'),
(16, 2, NULL, 'question1', 'description', 4, NULL, '2021-01-15 00:01:03', '2021-01-15 00:01:03'),
(17, 2, NULL, 'question1', 'description', 5, NULL, '2021-01-15 00:01:38', '2021-01-15 00:01:38'),
(18, 2, NULL, 'question1', 'description', 6, NULL, '2021-01-15 02:36:23', '2021-01-15 02:36:23'),
(19, 2, NULL, 'question1', 'description', 7, NULL, '2021-01-15 02:36:24', '2021-01-15 02:36:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question_evaluations`
--

CREATE TABLE `question_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_measure_evavaluation` bigint(20) UNSIGNED DEFAULT NULL,
  `id_question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `question_evaluations`
--

INSERT INTO `question_evaluations` (`id`, `id_measure_evavaluation`, `id_question`, `porcentage`, `commentary`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '20%', '1', '2021-04-08 11:27:52', '2021-04-08 11:27:52'),
(2, 2, '2', '20%', '1', '2021-04-08 11:29:40', '2021-04-08 11:29:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_answers`
--

CREATE TABLE `removed_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `removed_answer_type_id` bigint(20) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `porcentage` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_answer_types`
--

CREATE TABLE `removed_answer_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_blocks`
--

CREATE TABLE `removed_blocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `removed_tool_id` bigint(20) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `porcentage` double NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `removed_blocks`
--

INSERT INTO `removed_blocks` (`id`, `removed_tool_id`, `name`, `porcentage`, `description`, `position`, `created_at`, `updated_at`) VALUES
(17, 2, 'block1', 30, 'block1', 4, '2021-04-24 09:34:22', '2021-04-24 09:34:22'),
(18, 2, 'block1', 30, 'block1', 5, '2021-04-24 09:33:37', '2021-04-24 09:33:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_measures`
--

CREATE TABLE `removed_measures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `removed_block_id` bigint(20) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `porcentage` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_questions`
--

CREATE TABLE `removed_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `removed_measure_id` bigint(20) NOT NULL,
  `removed_answer_type_id` bigint(20) DEFAULT NULL,
  `name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `removed_tools`
--

CREATE TABLE `removed_tools` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `representatives`
--

CREATE TABLE `representatives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `representative_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identification_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issued_in` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `denomination_of_the_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `representatives`
--

INSERT INTO `representatives` (`id`, `contact_id`, `representative_name`, `identification_document`, `issued_in`, `denomination_of_the_position`, `created_at`, `updated_at`) VALUES
(1, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', '1234123', '2021-01-14 08:22:42', '2021-01-15 03:08:13'),
(2, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 08:22:43', '2021-01-14 08:22:43'),
(3, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 08:22:43', '2021-01-14 08:22:43'),
(4, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-15 03:07:44', '2021-01-15 03:07:44'),
(5, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-15 03:07:48', '2021-01-15 03:07:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_capitals`
--

CREATE TABLE `social_capitals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `capital` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exchange_rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `social_capitals`
--

INSERT INTO `social_capitals` (`id`, `company_id`, `capital`, `exchange_rate`, `created_at`, `updated_at`) VALUES
(1, 1, '11111', 'evwewvewv', '2021-01-14 08:19:16', '2021-04-10 16:17:14'),
(2, 1, '1', 'evwewvewv', '2021-01-14 08:21:08', '2021-01-14 08:21:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specific_limits`
--

CREATE TABLE `specific_limits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `representative_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `compentent_body` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mechanism` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `specific_limits`
--

INSERT INTO `specific_limits` (`id`, `representative_id`, `type`, `amount`, `compentent_body`, `mechanism`, `created_at`, `updated_at`) VALUES
(1, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', '234534534453', '2021-01-14 08:22:48', '2021-01-15 03:13:15'),
(2, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 08:22:49', '2021-01-14 08:22:49'),
(3, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 08:22:49', '2021-01-14 08:22:49'),
(4, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 08:22:50', '2021-01-14 08:22:50'),
(5, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 08:22:50', '2021-01-14 08:22:50'),
(6, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 08:22:51', '2021-01-14 08:22:51'),
(7, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-15 03:08:31', '2021-01-15 03:08:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `steps_transfer_of_fees`
--

CREATE TABLE `steps_transfer_of_fees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `step_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_limit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `steps_transfer_of_fees`
--

INSERT INTO `steps_transfer_of_fees` (`id`, `contact_id`, `step_name`, `description`, `time_limit`, `created_at`, `updated_at`) VALUES
(1, 1, '123', '234', 'dewedwdwedweedw', '2021-01-14 08:22:56', '2021-01-15 03:15:19'),
(2, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 08:22:57', '2021-01-14 08:22:57'),
(3, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 08:22:58', '2021-01-14 08:22:58'),
(4, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 08:22:58', '2021-01-14 08:22:58'),
(5, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 08:22:59', '2021-01-14 08:22:59'),
(6, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-15 03:14:23', '2021-01-15 03:14:23'),
(7, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-15 03:14:24', '2021-01-15 03:14:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_questions`
--

CREATE TABLE `sub_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `questions_id` bigint(20) UNSIGNED NOT NULL,
  `answer_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `porcentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sub_questions`
--

INSERT INTO `sub_questions` (`id`, `questions_id`, `answer_type_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '123456', 'description', 1, NULL, '2021-03-24 02:55:30', '2021-03-24 03:07:59'),
(2, 1, 2, '123456', 'description', 2, 20, '2021-03-24 03:05:54', '2021-04-28 03:53:41'),
(3, 1, NULL, 'question1', 'description', 3, NULL, '2021-03-24 03:05:55', '2021-03-24 03:05:55'),
(4, 1, NULL, 'question1', 'description', 4, NULL, '2021-03-24 03:05:55', '2021-03-24 03:05:55'),
(5, 1, NULL, 'question1', 'description', 5, NULL, '2021-03-24 03:05:56', '2021-03-24 03:05:56'),
(6, 1, NULL, 'question1', 'description', 6, NULL, '2021-03-24 03:05:56', '2021-03-24 03:05:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_question_evaluations`
--

CREATE TABLE `sub_question_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_question_evaluation` bigint(20) UNSIGNED DEFAULT NULL,
  `id_subquestion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sub_question_evaluations`
--

INSERT INTO `sub_question_evaluations` (`id`, `id_question_evaluation`, `id_subquestion`, `porcentage`, `commentary`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '20%', '1', '2021-04-08 11:32:44', '2021-04-08 11:32:44'),
(2, 2, '1', '20', '1', '2021-04-08 11:30:14', '2021-04-28 03:47:06'),
(3, 2, '3', '20%', '1', '2021-04-08 11:36:29', '2021-04-08 11:36:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tools`
--

CREATE TABLE `tools` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tools`
--

INSERT INTO `tools` (`id`, `user_id`, `name`, `description`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:09:53', '2020-12-05 07:09:53'),
(2, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:09:55', '2020-12-05 07:09:55'),
(3, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:09:56', '2020-12-05 07:09:56'),
(4, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:09:57', '2020-12-05 07:09:57'),
(5, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:09:59', '2020-12-05 07:09:59'),
(6, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:10:00', '2020-12-05 07:10:00'),
(7, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:10:01', '2020-12-05 07:10:01'),
(8, 1, 'herramienta123421', 'herramienta8', NULL, '2020-12-05 07:10:02', '2021-01-15 00:58:50'),
(9, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:10:12', '2020-12-05 07:10:12'),
(10, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 07:10:14', '2020-12-05 07:10:14'),
(11, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-15 00:52:25', '2021-01-15 00:52:25'),
(12, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-15 02:05:51', '2021-01-15 02:05:51'),
(13, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-15 02:05:52', '2021-01-15 02:05:52'),
(14, 1, 'herramienta1', 'herramienta1', NULL, '2021-04-12 10:13:01', '2021-04-12 10:13:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tool_evaluations`
--

CREATE TABLE `tool_evaluations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_evavaluation` bigint(20) UNSIGNED DEFAULT NULL,
  `id_tool` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tool_evaluations`
--

INSERT INTO `tool_evaluations` (`id`, `id_evavaluation`, `id_tool`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '20%', '2021-04-08 11:24:54', '2021-04-08 11:24:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `company_id`, `name`, `lastname`, `username`, `position`, `rol`, `email`, `email_verified_at`, `password`, `user_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'admin1', 'lastname1', 'admin1', 'abogado', 1, 'admin1@gmail.com', NULL, '$2y$10$LRL4Cz70JmhuILyTUPvj9uYYtRSp76ykkeVTl5LBky/Eoig.w0WMi', NULL, NULL, '2020-12-05 03:07:46', '2020-12-05 03:07:46'),
(2, NULL, 'admin2', 'lastname2', 'admin2', 'abogado', 2, 'admin2@gmail.com', NULL, '$2y$10$QQK1QPttoe/etRO5oQpD6ehZSj5P9SZ2FkFol9e2gvkFXaaFGC7Ty', NULL, NULL, '2021-01-20 05:49:40', '2021-01-20 05:49:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_permits`
--

CREATE TABLE `user_permits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED DEFAULT NULL,
  `id_tool` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votes_for_resolutions`
--

CREATE TABLE `votes_for_resolutions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `corporate_governance_regimes_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extraordinary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `votes_for_resolutions`
--

INSERT INTO `votes_for_resolutions` (`id`, `corporate_governance_regimes_id`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 08:23:05', '2021-01-14 08:23:05'),
(2, 2, '23', 'vdsvsdsdvvsddvs', '2021-01-14 08:23:06', '2021-01-15 03:22:29'),
(3, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 08:23:06', '2021-01-14 08:23:06'),
(4, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 08:23:07', '2021-01-14 08:23:07'),
(5, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:20:21', '2021-01-15 03:20:21'),
(6, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:20:29', '2021-01-15 03:20:29'),
(7, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:20:29', '2021-01-15 03:20:29'),
(8, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:20:30', '2021-01-15 03:20:30'),
(9, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:22:07', '2021-01-15 03:22:07'),
(10, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-15 03:22:07', '2021-01-15 03:22:07');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_answer_type_id_foreign` (`answer_type_id`);

--
-- Indices de la tabla `answer_types`
--
ALTER TABLE `answer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocks_tool_id_foreign` (`tool_id`);

--
-- Indices de la tabla `block_evaluations`
--
ALTER TABLE `block_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `block_evaluations_id_tool_evavaluation_foreign` (`id_tool_evavaluation`);

--
-- Indices de la tabla `call_to_assemblies`
--
ALTER TABLE `call_to_assemblies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `call_to_assemblies_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `capital_contribution_types`
--
ALTER TABLE `capital_contribution_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `capital_contribution_types_social_capitals_id_foreign` (`social_capitals_id`);

--
-- Indices de la tabla `contact_companies`
--
ALTER TABLE `contact_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `corporate_governance_regimes`
--
ALTER TABLE `corporate_governance_regimes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `corporate_governance_regimes_id_legal_reserves_foreign` (`id_legal_reserves`);

--
-- Indices de la tabla `dispute_resolutions`
--
ALTER TABLE `dispute_resolutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dispute_resolutions_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evaluations_company_id_foreign` (`company_id`);

--
-- Indices de la tabla `extra_company_datas`
--
ALTER TABLE `extra_company_datas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `extra_company_datas_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `global_answer_questions`
--
ALTER TABLE `global_answer_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_answer_questions_id_question_conf_foreign` (`id_question_conf`);

--
-- Indices de la tabla `global_answer_question_evaluations`
--
ALTER TABLE `global_answer_question_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_answer_question_evaluations_id_questioneva_foreign` (`id_questioneva`);

--
-- Indices de la tabla `global_answer_sub_questions`
--
ALTER TABLE `global_answer_sub_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_answer_sub_questions_id_sub_question_conf_foreign` (`id_sub_question_conf`);

--
-- Indices de la tabla `global_answer_sub_question_evaluations`
--
ALTER TABLE `global_answer_sub_question_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `global_answer_sub_question_evaluations_id_subquestioneva_foreign` (`id_subquestioneva`);

--
-- Indices de la tabla `legal_reserves`
--
ALTER TABLE `legal_reserves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `legal_reserves_participation_id_foreign` (`participation_id`);

--
-- Indices de la tabla `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `measures_block_id_foreign` (`block_id`);

--
-- Indices de la tabla `measure_evaluations`
--
ALTER TABLE `measure_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `measure_evaluations_id_block_evavaluation_foreign` (`id_block_evavaluation`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `participation_percentages`
--
ALTER TABLE `participation_percentages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participation_percentages_partners_id_foreign` (`partners_id`),
  ADD KEY `participation_percentages_social_capitals_id_foreign` (`social_capitals_id`);

--
-- Indices de la tabla `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partners_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `power_of_the_governing_bodies`
--
ALTER TABLE `power_of_the_governing_bodies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `power_of_the_governing_bodies_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_measure_id_foreign` (`measure_id`),
  ADD KEY `questions_answer_type_id_foreign` (`answer_type_id`);

--
-- Indices de la tabla `question_evaluations`
--
ALTER TABLE `question_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_evaluations_id_measure_evavaluation_foreign` (`id_measure_evavaluation`);

--
-- Indices de la tabla `removed_answers`
--
ALTER TABLE `removed_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `removed_answer_types`
--
ALTER TABLE `removed_answer_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `removed_blocks`
--
ALTER TABLE `removed_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `removed_measures`
--
ALTER TABLE `removed_measures`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `removed_questions`
--
ALTER TABLE `removed_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `removed_tools`
--
ALTER TABLE `removed_tools`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `representatives`
--
ALTER TABLE `representatives`
  ADD PRIMARY KEY (`id`),
  ADD KEY `representatives_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `social_capitals`
--
ALTER TABLE `social_capitals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_capitals_company_id_foreign` (`company_id`);

--
-- Indices de la tabla `specific_limits`
--
ALTER TABLE `specific_limits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specific_limits_representative_id_foreign` (`representative_id`);

--
-- Indices de la tabla `steps_transfer_of_fees`
--
ALTER TABLE `steps_transfer_of_fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `steps_transfer_of_fees_contact_id_foreign` (`contact_id`);

--
-- Indices de la tabla `sub_questions`
--
ALTER TABLE `sub_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_questions_questions_id_foreign` (`questions_id`),
  ADD KEY `sub_questions_answer_type_id_foreign` (`answer_type_id`);

--
-- Indices de la tabla `sub_question_evaluations`
--
ALTER TABLE `sub_question_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_question_evaluations_id_question_evaluation_foreign` (`id_question_evaluation`);

--
-- Indices de la tabla `tools`
--
ALTER TABLE `tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tools_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `tool_evaluations`
--
ALTER TABLE `tool_evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tool_evaluations_id_evavaluation_foreign` (`id_evavaluation`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_company_id_foreign` (`company_id`);

--
-- Indices de la tabla `user_permits`
--
ALTER TABLE `user_permits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_permits_id_user_foreign` (`id_user`);

--
-- Indices de la tabla `votes_for_resolutions`
--
ALTER TABLE `votes_for_resolutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `votes_for_resolutions_corporate_governance_regimes_id_foreign` (`corporate_governance_regimes_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `answer_types`
--
ALTER TABLE `answer_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `block_evaluations`
--
ALTER TABLE `block_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `call_to_assemblies`
--
ALTER TABLE `call_to_assemblies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `capital_contribution_types`
--
ALTER TABLE `capital_contribution_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `contact_companies`
--
ALTER TABLE `contact_companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `corporate_governance_regimes`
--
ALTER TABLE `corporate_governance_regimes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `dispute_resolutions`
--
ALTER TABLE `dispute_resolutions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `extra_company_datas`
--
ALTER TABLE `extra_company_datas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `global_answer_questions`
--
ALTER TABLE `global_answer_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `global_answer_question_evaluations`
--
ALTER TABLE `global_answer_question_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `global_answer_sub_questions`
--
ALTER TABLE `global_answer_sub_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `global_answer_sub_question_evaluations`
--
ALTER TABLE `global_answer_sub_question_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `legal_reserves`
--
ALTER TABLE `legal_reserves`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `measures`
--
ALTER TABLE `measures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `measure_evaluations`
--
ALTER TABLE `measure_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `participation_percentages`
--
ALTER TABLE `participation_percentages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `power_of_the_governing_bodies`
--
ALTER TABLE `power_of_the_governing_bodies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `question_evaluations`
--
ALTER TABLE `question_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `removed_answers`
--
ALTER TABLE `removed_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `removed_answer_types`
--
ALTER TABLE `removed_answer_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `removed_blocks`
--
ALTER TABLE `removed_blocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `removed_measures`
--
ALTER TABLE `removed_measures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `removed_questions`
--
ALTER TABLE `removed_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `removed_tools`
--
ALTER TABLE `removed_tools`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `representatives`
--
ALTER TABLE `representatives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `social_capitals`
--
ALTER TABLE `social_capitals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `specific_limits`
--
ALTER TABLE `specific_limits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `steps_transfer_of_fees`
--
ALTER TABLE `steps_transfer_of_fees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sub_questions`
--
ALTER TABLE `sub_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sub_question_evaluations`
--
ALTER TABLE `sub_question_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tools`
--
ALTER TABLE `tools`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tool_evaluations`
--
ALTER TABLE `tool_evaluations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_permits`
--
ALTER TABLE `user_permits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `votes_for_resolutions`
--
ALTER TABLE `votes_for_resolutions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_answer_type_id_foreign` FOREIGN KEY (`answer_type_id`) REFERENCES `answer_types` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `blocks`
--
ALTER TABLE `blocks`
  ADD CONSTRAINT `blocks_tool_id_foreign` FOREIGN KEY (`tool_id`) REFERENCES `tools` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `block_evaluations`
--
ALTER TABLE `block_evaluations`
  ADD CONSTRAINT `block_evaluations_id_tool_evavaluation_foreign` FOREIGN KEY (`id_tool_evavaluation`) REFERENCES `tool_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `call_to_assemblies`
--
ALTER TABLE `call_to_assemblies`
  ADD CONSTRAINT `call_to_assemblies_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `capital_contribution_types`
--
ALTER TABLE `capital_contribution_types`
  ADD CONSTRAINT `capital_contribution_types_social_capitals_id_foreign` FOREIGN KEY (`social_capitals_id`) REFERENCES `social_capitals` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `corporate_governance_regimes`
--
ALTER TABLE `corporate_governance_regimes`
  ADD CONSTRAINT `corporate_governance_regimes_id_legal_reserves_foreign` FOREIGN KEY (`id_legal_reserves`) REFERENCES `legal_reserves` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `dispute_resolutions`
--
ALTER TABLE `dispute_resolutions`
  ADD CONSTRAINT `dispute_resolutions_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `evaluations`
--
ALTER TABLE `evaluations`
  ADD CONSTRAINT `evaluations_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `extra_company_datas`
--
ALTER TABLE `extra_company_datas`
  ADD CONSTRAINT `extra_company_datas_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `global_answer_questions`
--
ALTER TABLE `global_answer_questions`
  ADD CONSTRAINT `global_answer_questions_id_question_conf_foreign` FOREIGN KEY (`id_question_conf`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `global_answer_question_evaluations`
--
ALTER TABLE `global_answer_question_evaluations`
  ADD CONSTRAINT `global_answer_question_evaluations_id_questioneva_foreign` FOREIGN KEY (`id_questioneva`) REFERENCES `question_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `global_answer_sub_questions`
--
ALTER TABLE `global_answer_sub_questions`
  ADD CONSTRAINT `global_answer_sub_questions_id_sub_question_conf_foreign` FOREIGN KEY (`id_sub_question_conf`) REFERENCES `sub_questions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `global_answer_sub_question_evaluations`
--
ALTER TABLE `global_answer_sub_question_evaluations`
  ADD CONSTRAINT `global_answer_sub_question_evaluations_id_subquestioneva_foreign` FOREIGN KEY (`id_subquestioneva`) REFERENCES `sub_question_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `legal_reserves`
--
ALTER TABLE `legal_reserves`
  ADD CONSTRAINT `legal_reserves_participation_id_foreign` FOREIGN KEY (`participation_id`) REFERENCES `participation_percentages` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `measures`
--
ALTER TABLE `measures`
  ADD CONSTRAINT `measures_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `measure_evaluations`
--
ALTER TABLE `measure_evaluations`
  ADD CONSTRAINT `measure_evaluations_id_block_evavaluation_foreign` FOREIGN KEY (`id_block_evavaluation`) REFERENCES `block_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `participation_percentages`
--
ALTER TABLE `participation_percentages`
  ADD CONSTRAINT `participation_percentages_partners_id_foreign` FOREIGN KEY (`partners_id`) REFERENCES `partners` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `participation_percentages_social_capitals_id_foreign` FOREIGN KEY (`social_capitals_id`) REFERENCES `social_capitals` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `partners`
--
ALTER TABLE `partners`
  ADD CONSTRAINT `partners_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `power_of_the_governing_bodies`
--
ALTER TABLE `power_of_the_governing_bodies`
  ADD CONSTRAINT `power_of_the_governing_bodies_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_answer_type_id_foreign` FOREIGN KEY (`answer_type_id`) REFERENCES `answer_types` (`id`),
  ADD CONSTRAINT `questions_measure_id_foreign` FOREIGN KEY (`measure_id`) REFERENCES `measures` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `question_evaluations`
--
ALTER TABLE `question_evaluations`
  ADD CONSTRAINT `question_evaluations_id_measure_evavaluation_foreign` FOREIGN KEY (`id_measure_evavaluation`) REFERENCES `measure_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `representatives`
--
ALTER TABLE `representatives`
  ADD CONSTRAINT `representatives_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `social_capitals`
--
ALTER TABLE `social_capitals`
  ADD CONSTRAINT `social_capitals_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `specific_limits`
--
ALTER TABLE `specific_limits`
  ADD CONSTRAINT `specific_limits_representative_id_foreign` FOREIGN KEY (`representative_id`) REFERENCES `representatives` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `steps_transfer_of_fees`
--
ALTER TABLE `steps_transfer_of_fees`
  ADD CONSTRAINT `steps_transfer_of_fees_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sub_questions`
--
ALTER TABLE `sub_questions`
  ADD CONSTRAINT `sub_questions_answer_type_id_foreign` FOREIGN KEY (`answer_type_id`) REFERENCES `answer_types` (`id`),
  ADD CONSTRAINT `sub_questions_questions_id_foreign` FOREIGN KEY (`questions_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sub_question_evaluations`
--
ALTER TABLE `sub_question_evaluations`
  ADD CONSTRAINT `sub_question_evaluations_id_question_evaluation_foreign` FOREIGN KEY (`id_question_evaluation`) REFERENCES `question_evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tools`
--
ALTER TABLE `tools`
  ADD CONSTRAINT `tools_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tool_evaluations`
--
ALTER TABLE `tool_evaluations`
  ADD CONSTRAINT `tool_evaluations_id_evavaluation_foreign` FOREIGN KEY (`id_evavaluation`) REFERENCES `evaluations` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `contact_companies` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_permits`
--
ALTER TABLE `user_permits`
  ADD CONSTRAINT `user_permits_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `votes_for_resolutions`
--
ALTER TABLE `votes_for_resolutions`
  ADD CONSTRAINT `votes_for_resolutions_corporate_governance_regimes_id_foreign` FOREIGN KEY (`corporate_governance_regimes_id`) REFERENCES `corporate_governance_regimes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
