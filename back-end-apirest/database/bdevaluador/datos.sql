
INSERT INTO `answers` (`id`, `answer_type_id`, `name`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(4, 2, 'Answer1', 1, 30, '2020-12-05 03:30:12', '2020-12-05 03:30:12'),
(5, 2, 'Answer1', 2, 30, '2020-12-05 03:30:13', '2020-12-05 03:30:13'),
(6, 2, 'Answer1', 3, 30, '2020-12-05 03:30:15', '2020-12-05 03:30:15'),
(7, 2, 'Answer7232323223', 4, 30, '2020-12-05 03:30:16', '2021-01-14 22:49:47'),
(8, 2, 'Answer1', 5, 30, '2020-12-05 03:30:17', '2020-12-05 03:30:17'),
(9, 2, 'Answer1', 6, 30, '2020-12-05 03:30:21', '2020-12-05 03:30:21'),
(10, 2, 'Answer1', 7, 30, '2020-12-05 03:30:25', '2020-12-05 03:30:25'),
(11, 3, 'Answer1', 1, 30, '2020-12-05 03:30:33', '2020-12-05 03:30:33'),
(12, 3, 'Answer1', 1, 30, '2020-12-05 03:30:34', '2020-12-05 03:30:34'),
(13, 3, 'Answer1', 2, 30, '2020-12-05 03:30:35', '2020-12-05 03:30:35'),
(14, 3, 'Answer1', 3, 30, '2020-12-05 03:31:22', '2020-12-05 03:31:22'),
(15, 3, 'Answer1', 5, 30, '2021-01-14 22:49:39', '2021-01-14 22:49:39');

INSERT INTO `answer_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Answer type1', '2020-12-05 03:19:55', '2020-12-05 03:19:55'),
(3, 'Answer type1', '2020-12-05 03:19:57', '2020-12-05 03:19:57'),
(4, 'Answer type1', '2020-12-05 03:19:58', '2020-12-05 03:19:58'),
(5, 'Answer type5', '2020-12-05 03:19:59', '2021-01-14 22:44:20'),
(6, 'Answer type1', '2020-12-05 03:20:00', '2020-12-05 03:20:00'),
(7, 'Answer type1', '2020-12-05 03:20:03', '2020-12-05 03:20:03'),
(8, 'Answer type1', '2020-12-05 03:20:06', '2020-12-05 03:20:06'),
(9, 'Answer type1', '2020-12-05 03:20:08', '2020-12-05 03:20:08'),
(10, 'Answer type1', '2020-12-05 03:20:09', '2020-12-05 03:20:09'),
(11, 'Answer type1', '2021-01-14 22:44:16', '2021-01-14 22:44:16'),
(12, 'Answer type1', '2021-01-14 22:44:16', '2021-01-14 22:44:16'),
(13, 'Answer type1', '2021-01-14 22:44:17', '2021-01-14 22:44:17'),
(14, 'Answer type1', '2021-01-14 22:50:13', '2021-01-14 22:50:13');

INSERT INTO `blocks` (`id`, `tool_id`, `name`, `porcentage`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, 2, 'block', 40, 'block', 1, '2020-12-05 03:12:06', '2021-04-26 22:33:29'),
(2, 1, 'block11', 30, 'block1', 1, '2020-12-05 03:12:14', '2021-04-24 02:37:55'),
(3, 3, 'block1', 30, 'block1', 3, '2020-12-05 03:12:18', '2020-12-05 03:12:18'),
(4, 4, 'block1', 30, 'block1', 4, '2020-12-05 03:12:22', '2020-12-05 03:12:22'),
(5, 1, 'block1', 30, 'block1', 2, '2020-12-05 03:12:26', '2020-12-06 04:09:12'),
(8, 1, '23e23e23', 30, 'block1', 4, '2020-12-05 03:12:30', '2021-01-14 22:11:02'),
(9, 1, 'block1', 30, 'block1', 5, '2020-12-05 03:12:32', '2020-12-08 16:22:17'),
(10, 1, 'block1', 30, 'block1', 6, '2020-12-05 03:12:33', '2020-12-08 16:22:17'),
(11, 1, 'block11', 30, 'block1', 3, '2020-12-08 16:16:16', '2021-01-14 19:54:27'),
(12, 1, 'block1', 30, 'block1', 7, '2020-12-08 16:18:44', '2020-12-08 16:22:17'),
(13, 1, 'block1', 30, 'block1', 8, '2020-12-08 16:20:14', '2020-12-08 16:22:17'),
(14, 1, 'block1', 30, 'block1', 9, '2020-12-08 16:22:34', '2020-12-08 16:22:34'),
(15, 2, 'block1', 30, 'block1', 2, '2020-12-08 16:24:46', '2020-12-08 16:24:46'),
(16, 2, 'block1', 30, 'block1', 3, '2021-01-14 22:09:54', '2021-01-14 22:09:54');

INSERT INTO `block_evaluations` (`id`, `id_tool_evavaluation`, `id_block`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '20%', '2021-04-08 07:25:36', '2021-04-08 07:25:36'),
(2, 1, '2', '20%', '2021-04-08 07:25:51', '2021-04-08 07:25:51'),
(3, 1, '3', '20%', '2021-04-08 07:26:07', '2021-04-08 07:26:07');

INSERT INTO `call_to_assemblies` (`id`, `contact_id`, `medium`, `anticipation_period`, `created_at`, `updated_at`) VALUES
(1, 1, '2342213321213213213', '12213', '2021-01-14 04:21:50', '2021-01-14 22:53:13'),
(2, 1, '2342', '12213', '2021-01-14 04:21:51', '2021-01-14 04:21:51'),
(3, 1, '2342', '12213', '2021-01-14 04:21:51', '2021-01-14 04:21:51'),
(4, 1, '2342', '12213', '2021-01-14 04:21:52', '2021-01-14 04:21:52'),
(5, 1, '2342', '12213', '2021-01-14 22:53:03', '2021-01-14 22:53:03');

INSERT INTO `capital_contribution_types` (`id`, `social_capitals_id`, `cash`, `equipment`, `moviliario`, `created_at`, `updated_at`) VALUES
(1, 1, '1200', '2132', '321231213', '2021-01-14 04:21:39', '2021-01-14 04:21:39'),
(2, 1, '1200', '2132', '321231213', '2021-01-14 04:21:40', '2021-01-14 04:21:40'),
(3, 1, '1200', '2132', '321231213', '2021-01-14 04:21:41', '2021-01-14 04:21:41'),
(4, 1, '1200', '2132', '321231213', '2021-01-14 04:21:42', '2021-01-14 04:21:42'),
(5, 1, '1200', '2132', '321231213', '2021-01-14 04:21:42', '2021-01-14 04:21:42'),
(6, 1, '1200', '2132', '321231213', '2021-01-14 04:21:43', '2021-01-14 04:21:43');

INSERT INTO `contact_companies` (`id`, `kind_of_society`, `business_name`, `trade_registration`, `id_tool`, `nit`, `type_of_item`, `date`, `legal_address`, `direction`, `contact_person`, `contact_number`, `contact_email`, `created_at`, `updated_at`) VALUES
(1, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', '2021-01-07 08:12:25', '2021-01-07 08:12:25'),
(2, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', '2021-01-07 08:12:47', '2021-01-07 08:12:47'),
(3, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', '2021-01-14 22:51:33', '2021-01-14 22:51:33'),
(4, 'weqwqe', 'ewwqe', 'qwqew', NULL, 'weqweq', 'qwewqe', 'weqwqe', 'ewqwqe', 'wqwqd', 'qwddwq', 'wqddwq', 'wqdwqd', '2021-04-12 04:33:11', '2021-04-12 04:33:11');

INSERT INTO `corporate_governance_regimes` (`id`, `id_legal_reserves`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 1, 'saassasa', 'qwdqw', '2021-01-14 04:22:18', '2021-01-14 04:22:18'),
(2, 1, 'saassasa', 'qwdqw', '2021-01-14 04:22:19', '2021-01-14 23:01:00'),
(3, 1, 'saassasa', 'qwdqw', '2021-01-14 04:22:19', '2021-01-14 04:22:19'),
(4, 1, 'saassasa', 'qwdqw', '2021-01-14 22:58:13', '2021-01-14 22:58:13'),
(5, 1, 'saassasa', 'qwdqw', '2021-01-14 22:58:15', '2021-01-14 22:58:15'),
(6, 1, 'saassasa', 'qwdqw', '2021-01-14 22:58:15', '2021-01-14 22:58:15');

INSERT INTO `dispute_resolutions` (`id`, `contact_id`, `instance`, `metod`, `regulation`, `type`, `campus`, `created_at`, `updated_at`) VALUES
(1, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 04:22:25', '2021-01-14 04:22:25'),
(2, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 04:22:26', '2021-01-14 04:22:26'),
(3, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 04:22:26', '2021-01-14 04:22:26'),
(4, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 04:22:27', '2021-01-14 04:22:27'),
(5, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 04:22:27', '2021-01-14 04:22:27'),
(6, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 23:02:17', '2021-01-14 23:02:17'),
(7, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 23:02:20', '2021-01-14 23:02:20'),
(8, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 23:02:21', '2021-01-14 23:02:21'),
(9, 1, 'qwddwqwqd', 'dwqwqdwqd', 'wqdwqdwdq', 'dwqwqdwqd', 'qdwwqdqwd', '2021-01-14 23:02:22', '2021-01-14 23:02:22');

INSERT INTO `evaluations` (`id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-04-08 07:24:46', '2021-04-08 07:24:46');

INSERT INTO `extra_company_datas` (`id`, `contact_id`, `data_name`, `data`, `type`, `url`, `created_at`, `updated_at`) VALUES
(3, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 12:02:00', '2021-04-02 12:02:00'),
(4, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 12:02:01', '2021-04-02 12:02:01'),
(5, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 12:02:01', '2021-04-02 12:02:01'),
(6, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-02 12:02:02', '2021-04-02 12:02:02'),
(7, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 06:17:04', '2021-04-12 06:17:04'),
(8, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 06:28:34', '2021-04-12 06:28:34'),
(9, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 06:33:05', '2021-04-12 06:33:05'),
(10, 1, 'nombre de dato', 'dato', NULL, NULL, '2021-04-12 06:47:54', '2021-04-12 06:47:54');

INSERT INTO `global_answer_questions` (`id`, `id_question_conf`, `id_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:02:39', '2021-04-23 22:02:39'),
(2, 1, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:02:42', '2021-04-23 22:02:42'),
(3, 1, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:02:42', '2021-04-23 22:02:42');

INSERT INTO `global_answer_sub_questions` (`id`, `id_sub_question_conf`, `id_sub_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 3, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:03:20', '2021-04-23 22:03:20'),
(2, 3, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:03:21', '2021-04-23 22:03:21'),
(3, 3, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:03:21', '2021-04-23 22:03:21'),
(4, 3, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:03:22', '2021-04-23 22:03:22'),
(5, 3, '2', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 22:03:22', '2021-04-23 22:03:22');

INSERT INTO `global_answer_sub_question_evaluations` (`id`, `id_subquestioneva`, `id_sub_question`, `name`, `id_type_answer`, `id_answer`, `porcentage`, `sub_answer`, `need`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 21:59:20', '2021-04-23 21:59:20'),
(2, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 21:59:56', '2021-04-23 21:59:56'),
(3, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 21:59:57', '2021-04-23 21:59:57'),
(4, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 21:59:57', '2021-04-23 21:59:57'),
(5, 1, '1', 'sdfsd', '1', '2', '20', 'iuqbcuiqwuicuiqwc qnwxinqwio', 'iuqbcuiqwuicuiqwc qnwxinqwio', '2021-04-23 21:59:58', '2021-04-23 21:59:58');

INSERT INTO `legal_reserves` (`id`, `participation_id`, `legal_reserve_fund`, `annual_percentage_retained`, `created_at`, `updated_at`) VALUES
(1, 1, '123132321231213', '4342234', '2021-01-14 04:21:58', '2021-01-14 22:58:03'),
(2, 1, '322332', '4342234', '2021-01-14 04:21:59', '2021-01-14 04:21:59'),
(3, 1, '322332', '4342234', '2021-01-14 04:21:59', '2021-01-14 04:21:59'),
(4, 1, '322332', '4342234', '2021-01-14 04:22:00', '2021-01-14 04:22:00'),
(5, 1, '322332', '4342234', '2021-01-14 22:57:05', '2021-01-14 22:57:05');

INSERT INTO `measures` (`id`, `block_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, 'measure2oqwndoiqmwoimwq', 'measure2cwqqwcqwcwqwqcwcqwcq', 1, 10, '2020-12-05 03:14:55', '2021-04-24 23:57:04'),
(2, 1, 'measure2', 'measure2', 1, 30, '2020-12-05 03:15:03', '2020-12-08 16:26:10'),
(3, 3, 'measure2', 'measure2', 3, 30, '2020-12-05 03:15:07', '2020-12-05 03:15:07'),
(4, 4, 'measure2', 'measure2', 4, 30, '2020-12-05 03:15:11', '2020-12-05 03:15:11'),
(5, 5, 'measure2', 'measure2', 5, 30, '2020-12-05 03:15:16', '2020-12-05 03:15:16'),
(6, 1, 'measure6', 'measure6', 2, 30, '2020-12-05 03:15:21', '2020-12-08 16:26:10'),
(7, 1, 'measure2', 'measure2', 3, 30, '2020-12-05 03:15:22', '2020-12-08 16:26:10'),
(8, 1, 'measure2', 'measure2', 5, 30, '2020-12-05 03:15:24', '2020-12-08 16:26:10'),
(10, 1, 'measure2', 'measure2', 6, 30, '2020-12-05 03:15:26', '2020-12-08 16:26:10'),
(11, 1, 'measure2', 'measure2', 4, 30, '2020-12-08 16:25:39', '2020-12-08 16:26:10'),
(12, 1, 'measure2', 'measure2', 7, 30, '2020-12-08 16:26:50', '2020-12-08 16:26:50'),
(13, 1, 'measure2', 'measure2', 8, 30, '2020-12-08 16:26:52', '2020-12-08 16:26:52'),
(14, 2, 'measure', 'measure', 2, 70, '2020-12-08 16:27:23', '2021-04-27 03:34:06'),
(15, 2, 'measure2', 'measure2', 3, 30, '2021-01-14 22:33:21', '2021-01-14 22:33:21');

INSERT INTO `measure_evaluations` (`id`, `id_block_evavaluation`, `id_measure`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 3, '1', '20%', '2021-04-08 07:26:48', '2021-04-08 07:26:48'),
(2, 3, '2', '20%', '2021-04-08 07:27:33', '2021-04-08 07:27:33');


INSERT INTO `participation_percentages` (`id`, `partners_id`, `social_capitals_id`, `input`, `capital_quota`, `participation`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 04:21:23', '2021-01-14 04:21:23'),
(2, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 04:21:26', '2021-01-14 04:21:26'),
(3, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 04:21:27', '2021-01-14 04:21:27'),
(4, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 04:21:29', '2021-01-14 04:21:29'),
(5, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 22:52:33', '2021-01-14 22:52:33'),
(6, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 22:52:47', '2021-01-14 22:52:47'),
(7, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 22:52:47', '2021-01-14 22:52:47'),
(8, 1, 1, 'wefeqf1111111', 'evwewvewv', 'vewewvewv', '2021-01-14 22:52:48', '2021-01-14 22:52:48');

INSERT INTO `partners` (`id`, `contact_id`, `name`, `identification_document`, `issued_in`, `attorney`, `instrument_power`, `created_at`, `updated_at`) VALUES
(1, 1, 'adwqwqddwq', '1231231', NULL, 'qwwqwqwq', NULL, '2021-01-07 08:36:53', '2021-01-07 08:41:34'),
(2, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', NULL, 'qwwqwqwq', NULL, '2021-01-07 08:37:25', '2021-01-07 08:37:25'),
(3, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', NULL, 'qwwqwqwq', NULL, '2021-01-07 08:37:26', '2021-01-07 08:37:26'),
(4, 1, 'adwqwqddwq', 'wqdwqdqwdwqd', 'wqdwqdqwdwqd', 'wqdwqdqwdwqd', 'qwwqwqwq', '2021-01-14 22:52:13', '2021-01-14 22:52:13');

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'authToken', 'd0c8bf36ba52cfa0ff70f3dfec8e481578ff4f2fc732fcd76151d637cd317e85', '[\"*\"]', '2021-04-27 03:34:12', '2020-12-04 23:08:07', '2021-04-27 03:34:12');

INSERT INTO `power_of_the_governing_bodies` (`id`, `contact_id`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 1, '213423', 'dqwqwdwdqwwqd', '2021-01-14 04:22:33', '2021-01-14 23:05:07'),
(2, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 04:22:34', '2021-01-14 04:22:34'),
(3, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 04:22:35', '2021-01-14 04:22:35'),
(4, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 04:22:36', '2021-01-14 04:22:36'),
(5, 1, 'dwqdwqwqddwq', 'dqwqwdwdqwwqd', '2021-01-14 04:22:37', '2021-01-14 04:22:37');

INSERT INTO `questions` (`id`, `measure_id`, `answer_type_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 4, NULL, '1qew1231231231231231', 'description', 1, NULL, '2020-12-05 03:21:18', '2021-01-14 22:39:30'),
(2, 4, 2, 'question1', 'description', 2, NULL, '2020-12-05 03:21:21', '2020-12-05 03:21:21'),
(3, 1, NULL, '1qewq', '234', 3, NULL, '2020-12-05 03:21:25', '2021-01-14 20:25:16'),
(4, 1, NULL, 'question1', 'description', 4, NULL, '2020-12-05 03:21:26', '2020-12-06 04:14:54'),
(5, 1, NULL, 'question1', 'description', 5, NULL, '2020-12-05 03:21:27', '2020-12-06 04:14:55'),
(6, 1, NULL, 'question1', 'description', 6, NULL, '2020-12-05 03:21:28', '2020-12-06 04:14:55'),
(7, 1, NULL, 'question1', 'description', 7, NULL, '2020-12-05 03:21:30', '2020-12-06 04:14:55'),
(8, 1, NULL, 'question1', 'description', 8, NULL, '2020-12-05 03:21:31', '2020-12-06 04:14:55'),
(9, 3, NULL, 'question1', 'description', 9, NULL, '2020-12-05 03:21:35', '2020-12-06 04:14:55'),
(10, 3, NULL, 'question1', 'description', 10, NULL, '2020-12-05 03:21:36', '2020-12-06 04:14:55'),
(11, 3, NULL, 'question1', 'description', 11, NULL, '2020-12-05 03:21:37', '2020-12-06 04:14:55'),
(12, 3, NULL, 'question1', 'description', 12, NULL, '2020-12-05 03:21:38', '2020-12-06 04:14:55'),
(13, 2, NULL, 'question1', 'description', 13, NULL, '2020-12-05 03:21:43', '2020-12-06 04:14:55'),
(14, 2, NULL, 'question1', 'description', 14, NULL, '2020-12-05 03:21:44', '2020-12-06 04:14:55'),
(15, 2, NULL, 'question1', 'description', 3, NULL, '2021-01-14 19:58:20', '2021-01-14 19:58:20'),
(16, 2, NULL, 'question1', 'description', 4, NULL, '2021-01-14 20:01:03', '2021-01-14 20:01:03'),
(17, 2, NULL, 'question1', 'description', 5, NULL, '2021-01-14 20:01:38', '2021-01-14 20:01:38'),
(18, 2, NULL, 'question1', 'description', 6, NULL, '2021-01-14 22:36:23', '2021-01-14 22:36:23'),
(19, 2, NULL, 'question1', 'description', 7, NULL, '2021-01-14 22:36:24', '2021-01-14 22:36:24');

INSERT INTO `question_evaluations` (`id`, `id_measure_evavaluation`, `id_question`, `porcentage`, `commentary`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '20%', '1', '2021-04-08 07:27:52', '2021-04-08 07:27:52'),
(2, 2, '2', '20%', '1', '2021-04-08 07:29:40', '2021-04-08 07:29:40');

INSERT INTO `removed_blocks` (`id`, `removed_tool_id`, `name`, `porcentage`, `description`, `position`, `created_at`, `updated_at`) VALUES
(17, 2, 'block1', 30, 'block1', 4, '2021-04-24 05:34:22', '2021-04-24 05:34:22'),
(18, 2, 'block1', 30, 'block1', 5, '2021-04-24 05:33:37', '2021-04-24 05:33:37');

INSERT INTO `representatives` (`id`, `contact_id`, `representative_name`, `identification_document`, `issued_in`, `denomination_of_the_position`, `created_at`, `updated_at`) VALUES
(1, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', '1234123', '2021-01-14 04:22:42', '2021-01-14 23:08:13'),
(2, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 04:22:43', '2021-01-14 04:22:43'),
(3, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 04:22:43', '2021-01-14 04:22:43'),
(4, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 23:07:44', '2021-01-14 23:07:44'),
(5, 1, 'wqdfqdwdwqqdw', 'asdasdasd', 'asdasdasd', 'asdasdasdasda', '2021-01-14 23:07:48', '2021-01-14 23:07:48');

INSERT INTO `social_capitals` (`id`, `company_id`, `capital`, `exchange_rate`, `created_at`, `updated_at`) VALUES
(1, 1, '11111', 'evwewvewv', '2021-01-14 04:19:16', '2021-04-10 12:17:14'),
(2, 1, '1', 'evwewvewv', '2021-01-14 04:21:08', '2021-01-14 04:21:08');

INSERT INTO `specific_limits` (`id`, `representative_id`, `type`, `amount`, `compentent_body`, `mechanism`, `created_at`, `updated_at`) VALUES
(1, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', '234534534453', '2021-01-14 04:22:48', '2021-01-14 23:13:15'),
(2, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 04:22:49', '2021-01-14 04:22:49'),
(3, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 04:22:49', '2021-01-14 04:22:49'),
(4, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 04:22:50', '2021-01-14 04:22:50'),
(5, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 04:22:50', '2021-01-14 04:22:50'),
(6, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 04:22:51', '2021-01-14 04:22:51'),
(7, 1, 'dwqdwqdwq', 'wdqwqdwqd', 'qdwwqdwqdwd', 'qdwdwqqdwdwqdwqqdw', '2021-01-14 23:08:31', '2021-01-14 23:08:31');

INSERT INTO `steps_transfer_of_fees` (`id`, `contact_id`, `step_name`, `description`, `time_limit`, `created_at`, `updated_at`) VALUES
(1, 1, '123', '234', 'dewedwdwedweedw', '2021-01-14 04:22:56', '2021-01-14 23:15:19'),
(2, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 04:22:57', '2021-01-14 04:22:57'),
(3, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 04:22:58', '2021-01-14 04:22:58'),
(4, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 04:22:58', '2021-01-14 04:22:58'),
(5, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 04:22:59', '2021-01-14 04:22:59'),
(6, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 23:14:23', '2021-01-14 23:14:23'),
(7, 1, '213231213wewe', 'dewdewdewedw', 'dewedwdwedweedw', '2021-01-14 23:14:24', '2021-01-14 23:14:24');

INSERT INTO `sub_questions` (`id`, `questions_id`, `answer_type_id`, `name`, `description`, `position`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '123456', 'description', 1, NULL, '2021-03-23 22:55:30', '2021-03-23 23:07:59'),
(2, 1, NULL, 'question1', 'description', 2, NULL, '2021-03-23 23:05:54', '2021-03-23 23:05:54'),
(3, 1, NULL, 'question1', 'description', 3, NULL, '2021-03-23 23:05:55', '2021-03-23 23:05:55'),
(4, 1, NULL, 'question1', 'description', 4, NULL, '2021-03-23 23:05:55', '2021-03-23 23:05:55'),
(5, 1, NULL, 'question1', 'description', 5, NULL, '2021-03-23 23:05:56', '2021-03-23 23:05:56'),
(6, 1, NULL, 'question1', 'description', 6, NULL, '2021-03-23 23:05:56', '2021-03-23 23:05:56');

INSERT INTO `sub_question_evaluations` (`id`, `id_question_evaluation`, `id_subquestion`, `porcentage`, `commentary`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '20%', '1', '2021-04-08 07:32:44', '2021-04-08 07:32:44'),
(2, 2, '1', '20%', '1', '2021-04-08 07:30:14', '2021-04-08 07:30:14'),
(3, 2, '3', '20%', '1', '2021-04-08 07:36:29', '2021-04-08 07:36:29');

INSERT INTO `tools` (`id`, `user_id`, `name`, `description`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:09:53', '2020-12-05 03:09:53'),
(2, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:09:55', '2020-12-05 03:09:55'),
(3, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:09:56', '2020-12-05 03:09:56'),
(4, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:09:57', '2020-12-05 03:09:57'),
(5, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:09:59', '2020-12-05 03:09:59'),
(6, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:10:00', '2020-12-05 03:10:00'),
(7, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:10:01', '2020-12-05 03:10:01'),
(8, 1, 'herramienta123421', 'herramienta8', NULL, '2020-12-05 03:10:02', '2021-01-14 20:58:50'),
(9, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:10:12', '2020-12-05 03:10:12'),
(10, 1, 'herramienta1', 'herramienta1', NULL, '2020-12-05 03:10:14', '2020-12-05 03:10:14'),
(11, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-14 20:52:25', '2021-01-14 20:52:25'),
(12, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-14 22:05:51', '2021-01-14 22:05:51'),
(13, 1, 'herramienta1', 'herramienta1', NULL, '2021-01-14 22:05:52', '2021-01-14 22:05:52'),
(14, 1, 'herramienta1', 'herramienta1', NULL, '2021-04-12 06:13:01', '2021-04-12 06:13:01');

INSERT INTO `tool_evaluations` (`id`, `id_evavaluation`, `id_tool`, `porcentage`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '20%', '2021-04-08 07:24:54', '2021-04-08 07:24:54');


INSERT INTO `users` (`id`, `company_id`, `name`, `lastname`, `username`, `position`, `rol`, `email`, `email_verified_at`, `password`, `user_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'admin1', 'lastname1', 'admin1', 'abogado', 1, 'admin1@gmail.com', NULL, '$2y$10$LRL4Cz70JmhuILyTUPvj9uYYtRSp76ykkeVTl5LBky/Eoig.w0WMi', NULL, NULL, '2020-12-04 23:07:46', '2020-12-04 23:07:46'),
(2, NULL, 'admin2', 'lastname2', 'admin2', 'abogado', 2, 'admin2@gmail.com', NULL, '$2y$10$QQK1QPttoe/etRO5oQpD6ehZSj5P9SZ2FkFol9e2gvkFXaaFGC7Ty', NULL, NULL, '2021-01-20 01:49:40', '2021-01-20 01:49:40');

INSERT INTO `votes_for_resolutions` (`id`, `corporate_governance_regimes_id`, `ordinary`, `extraordinary`, `created_at`, `updated_at`) VALUES
(1, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 04:23:05', '2021-01-14 04:23:05'),
(2, 2, '23', 'vdsvsdsdvvsddvs', '2021-01-14 04:23:06', '2021-01-14 23:22:29'),
(3, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 04:23:06', '2021-01-14 04:23:06'),
(4, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 04:23:07', '2021-01-14 04:23:07'),
(5, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:20:21', '2021-01-14 23:20:21'),
(6, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:20:29', '2021-01-14 23:20:29'),
(7, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:20:29', '2021-01-14 23:20:29'),
(8, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:20:30', '2021-01-14 23:20:30'),
(9, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:22:07', '2021-01-14 23:22:07'),
(10, 2, 'erggsesgsdvgdsfdsvvds', 'vdsvsdsdvvsddvs', '2021-01-14 23:22:07', '2021-01-14 23:22:07');
