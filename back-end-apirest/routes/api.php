<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\BlockController;
use App\Http\Controllers\MeasureController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\ExtraCompanyDataController;
use App\Http\Controllers\SocialCapitalController;
use App\Http\Controllers\DisputeResolutionController;
use App\Http\Controllers\PowerOfTheGoverningBodyController;
use App\Http\Controllers\StepsTransferOfFeesController;
use App\Http\Controllers\CallToAssemblyController;
use App\Http\Controllers\RepresentativeController;
use App\Http\Controllers\PartnersController;
use App\Http\Controllers\GlobalAnswerQuestionController;
use App\Http\Controllers\GlobalAnswerSubQuestionController;

use App\Http\Controllers\ChangePasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
    Route::post('/register',[AuthController::class,'register']);
    Route::post('/login',[AuthController::class,'login']); 
});

$router->group(['middleware' => ['cors','auth:sanctum']], function (){
    Route::resource('tool', 'App\Http\Controllers\ToolController', ['except' => ['create', 'edit']]);
    Route::get('/getAllDataTool/{id}',[ToolController::class,'getAllDataTool']); 

    Route::resource('block', 'App\Http\Controllers\BlockController', ['except' => ['create', 'edit']]);
    Route::get('/getBlockOfTool/{id}',[BlockController::class,'getBlockOfTool']); 
    Route::get('/getBlockWithPosition/{pos}/{id_tool}',[BlockController::class,'getBlockWithPosition']); 

    Route::resource('measure', 'App\Http\Controllers\MeasureController', ['except' => ['create', 'edit']]);
    Route::get('/getMeasureOfBlock/{id}',[MeasureController::class,'getMeasureOfBlock']); 
    Route::get('/getMeasureWithPosition/{pos}/{id_block}',[MeasureController::class,'getMeasureWithPosition']); 

    Route::resource('question', 'App\Http\Controllers\QuestionController', ['except' => ['create', 'edit']]);
    Route::get('/getQuestionOfMeasure/{id}',[QuestionController::class,'getQuestionOfMeasure']); 
    Route::get('/getQuiestionWithPosition/{pos}/{id_measure}',[QuestionController::class,'getQuiestionWithPosition']); 

    Route::resource('subQuestion', 'App\Http\Controllers\SubQuestionsController', ['except' => ['create', 'edit']]);

    Route::resource('answerType', 'App\Http\Controllers\AnswerTypeController', ['except' => ['create', 'edit']]);
    
    Route::resource('answer', 'App\Http\Controllers\AnswerController', ['except' => ['create', 'edit']]);
    Route::get('/getAnswerOfAnswerType/{id}',[AnswerController::class,'getAnswerOfAnswerType']); 
    Route::get('/getAnswerWithPosition/{pos}/{id_answer_type}',[AnswerController::class,'getAnswerWithPosition']);
    
    Route::resource('company', 'App\Http\Controllers\ContactCompanyController', ['except' => ['create', 'edit']]); 

    Route::resource('extradataCompany', 'App\Http\Controllers\ExtraCompanyDataController', ['except' => ['create', 'edit']]);
    Route::get('/getExtraDataContact/{id_company}',[ExtraCompanyDataController::class,'getExtraDataContact']);

    Route::resource('partners', 'App\Http\Controllers\PartnersController', ['except' => ['create', 'edit']]);
    Route::get('/getPartnersContact/{id_company}',[PartnersController::class,'getPartnersContact']);
    
    Route::resource('participationPercentage', 'App\Http\Controllers\ParticipationPercentageController', ['except' => ['create', 'edit']]);

    Route::resource('socialCapital', 'App\Http\Controllers\SocialCapitalController', ['except' => ['create', 'edit']]);
    Route::get('/getSocialCapitalContact/{id_company}',[SocialCapitalController::class,'getSocialCapitalContact']);

    Route::resource('capitalContributionType', 'App\Http\Controllers\CapitalContributionTypeController', ['except' => ['create', 'edit']]);

    Route::resource('callToAssembly', 'App\Http\Controllers\CallToAssemblyController', ['except' => ['create', 'edit']]);
    Route::get('/getCallToAssembly/{id_company}',[CallToAssemblyController::class,'getCallToAssembly']);
    
    Route::resource('corporateGovernanceRegime', 'App\Http\Controllers\CorporateGovernanceRegimeController', ['except' => ['create', 'edit']]);

    Route::resource('legalReserve', 'App\Http\Controllers\LegalReserveController', ['except' => ['create', 'edit']]);
    
    Route::resource('disputeResolution', 'App\Http\Controllers\DisputeResolutionController', ['except' => ['create', 'edit']]);
    Route::get('/getDisputeResolution/{id_company}',[DisputeResolutionController::class,'getDisputeResolution']);

    Route::resource('powerOfTheGoverningBody', 'App\Http\Controllers\PowerOfTheGoverningBodyController', ['except' => ['create', 'edit']]);
    Route::get('/getPowerOfTheGoverningBody/{id_company}',[PowerOfTheGoverningBodyController::class,'getPowerOfTheGoverningBody']);

    Route::resource('representative', 'App\Http\Controllers\RepresentativeController', ['except' => ['create', 'edit']]);
    Route::get('/getRepresentative/{id_company}',[RepresentativeController::class,'getRepresentative']);

    Route::resource('specificLimits', 'App\Http\Controllers\SpecificLimitsController', ['except' => ['create', 'edit']]);

    Route::resource('stepsTransferOfFees', 'App\Http\Controllers\StepsTransferOfFeesController', ['except' => ['create', 'edit']]);
    Route::get('/getStepsTransferOfFees/{id_company}',[StepsTransferOfFeesController::class,'getStepsTransferOfFees']);
    
    Route::resource('votesForResolution', 'App\Http\Controllers\VotesForResolutionController', ['except' => ['create', 'edit']]);

    Route::resource('evaluation', 'App\Http\Controllers\EvaluationController', ['except' => ['create', 'edit']]);
    
    Route::resource('toolEvaluation', 'App\Http\Controllers\ToolEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('blockEvaluation', 'App\Http\Controllers\BlockEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('measureEvaluation', 'App\Http\Controllers\MeasureEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('questionEvaluation', 'App\Http\Controllers\QuestionEvaluationController', ['except' => ['create', 'edit']]);
    
    Route::resource('subQuestionEvaluation', 'App\Http\Controllers\SubQuestionEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('answerEvaluation', 'App\Http\Controllers\AnswerEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('globalAnswerQuestion', 'App\Http\Controllers\GlobalAnswerQuestionController', ['except' => ['create', 'edit']]);
    Route::get('/globalAnswersByQuestion/{id}',[GlobalAnswerQuestionController::class,'getAnswerByQuestion']);
    
    Route::resource('globalAnswersSubQuestion', 'App\Http\Controllers\GlobalAnswerSubQuestionController', ['except' => ['create', 'edit']]);
    Route::get('/globalAnswersBySubQuestion/{id}',[GlobalAnswerSubQuestionController::class,'getAnswerBySubQuestion']);

    Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['create', 'edit']]);
    
    Route::resource('userPermits', 'App\Http\Controllers\UserPermitsController', ['except' => ['create', 'edit']]);

    Route::resource('globalAnswerQuestionEvaluation', 'App\Http\Controllers\GlobalAnswerQuestionEvaluationController', ['except' => ['create', 'edit']]);

    Route::resource('globalAnswerSubQuestionEvaluation', 'App\Http\Controllers\GlobalAnswerSubQuestionEvaluationController', ['except' => ['create', 'edit']]);


    //User
    Route::post('/changePassword/{id_user}',[ChangePasswordController::class,'change_password']);
    Route::post('/logout',[AuthController::class,'logout']);
});




