<?php

namespace App\Http\Controllers;
use App\Http\Controllers\QuestionController;

use App\Models\Question;
use App\Models\Answer_type;
use App\Models\Answer;

use App\Models\Removed_answer_type;
use App\Models\Removed_answer;

use Illuminate\Http\Request;

class AnswerTypeController extends Controller
{

    public function index()
    {
        $answer_type = Answer_type::all();
        if ($answer_type != '[]'){
            return json_encode($answer_type);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Tipos de respuesta',
            ]);
        }
    }

    public function store(Request $request)
    {
        $answer_type = new Answer_type();
        $answer_type->name = $request->input ('name');
        $answer_type->save();
        return response()->json([
            'status_code' => 200,
            'id' => $answer_type->id,
            'name' => $answer_type->name,
        ]);
    }

    public function show($id)
    {
        $answer_type = Answer_type::find($id);
        if ($answer_type != '[]' && $answer_type != null){
            return json_encode($answer_type);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Tipos de respuesta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $answer_type = Answer_type::find($id);
        if ($answer_type != null){
            $answer_type->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensage' => "Tipo de respuesta actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Tipo de respuesta no encontrado",
            ]);
        }
        
    }
    
    public function destroy($id)
    {
        $answer_type = Answer_type::find($id);
        if ($answer_type != null){

            //Elimanar la dependencia de las preguntas
            $questionController = new QuestionController;
            $questions = Question::where('answer_type_id', 'like' , $answer_type['id'])->get();
            foreach ($questions as $oneQuestion){
                $requestObj = new Request(array('answer_type_id' => null));
                $questionController->update($requestObj,$oneQuestion['id']);
            }

            $answerTypeRemoved = new Removed_answer_type();
            $answerTypeRemoved->id = $answer_type['id'];
            $answerTypeRemoved->name = $answer_type['name'];
            $answerTypeRemoved->save();
            $answer = Answer::where('answer_type_id', 'like' , $answer_type['id'])->get();
            foreach ($answer as $datosAnswer) {
                $answerRemoved = new Removed_answer();
                $answerRemoved->id = $datosAnswer['id'];
                $answerRemoved->removed_answer_type_id = $datosAnswer['answer_type_id'];
                $answerRemoved->name = $datosAnswer['name'];
                $answerRemoved->position = $datosAnswer['position'];
                $answerRemoved->porcentage = $datosAnswer['porcentage'];
                $answerRemoved->save();
            }

            $answer_type->delete();
            return response()->json([
                'status_code' => 200,
                'mensage' => "Medida eliminada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Medida no encontrado",
            ]);
        }
    }
}
