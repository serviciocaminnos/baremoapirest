<?php

namespace App\Http\Controllers;

use App\Models\Extra_company_data;
use Illuminate\Http\Request;

class ExtraCompanyDataController extends Controller
{
    public function index()
    {
        $extra_company_data = Extra_company_data::all();
        if ($extra_company_data != '[]'){
            return json_encode($extra_company_data);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen datos extra',
            ]);
        }
    }

    public function getExtraDataContact ($id_company){
        $extra_company_data = Extra_company_data::where('contact_id', 'like' , $id_company)->get();
        if ($extra_company_data != '[]'){
            return json_encode($extra_company_data);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen datos extra',
            ]);
        }
    }

    public function store(Request $request)
    {
        $extra_company_data = new Extra_company_data();
        $extra_company_data->contact_id = $request->input ('contact_id');
        $extra_company_data->data_name = $request->input ('data_name');
        $extra_company_data->data = $request->input ('data');
        $extra_company_data->type = $request->input ('type');
        $extra_company_data->url = $request->input ('url');
        $extra_company_data->save();
        return response()->json([
            'status_code' => 200,
            'extra_company_data' => $extra_company_data->id,
            'contact_id' => $extra_company_data->contact_id,
        ]);
    }

    public function show($id)
    {
        $extra_company_data = Extra_company_data::find($id);
        if ($extra_company_data != null){
            return json_encode($extra_company_data);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe dato extra',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $extra_company_data = Extra_company_data::find($id);
        if ($extra_company_data != null){
            $extra_company_data->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Dato extra actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Dato extra empresa no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $extra_company_data = Extra_company_data::find($id);
        if ($extra_company_data != null){
            $id_dato_extra = $extra_company_data['id'];
            $extra_company_data->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => " Dato extra eliminado correctamente",
                'id_dato_extra' => $id_dato_extra
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "dato extra no encontrado",
            ]);
        }
    }
}
