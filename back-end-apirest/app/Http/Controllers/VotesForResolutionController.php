<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Votes_for_resolution;
use Illuminate\Http\Request;

class VotesForResolutionController extends Controller
{
    public function index()
    {
        $votes_for_resolution = Votes_for_resolution::all();
        if ($votes_for_resolution != '[]'){
            return json_encode($votes_for_resolution);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Votos de resolucion',
            ]);
        }
    }

    public function store(Request $request)
    {
        $votes_for_resolution = new Votes_for_resolution();
        $votes_for_resolution->corporate_governance_regimes_id = $request->input ('corporate_governance_regimes_id');
        $votes_for_resolution->ordinary = $request->input ('ordinary');
        $votes_for_resolution->extraordinary = $request->input ('extraordinary');
        $votes_for_resolution->save();
        return response()->json([
            'status_code' => 200,
            'Votes_for_resolution_id' => $votes_for_resolution->id,
        ]);
    }

    public function show($id)
    {
        $votes_for_resolution = Votes_for_resolution::find($id);
        if ($votes_for_resolution != null){
            return json_encode($votes_for_resolution);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Votos de resolucion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $votes_for_resolution = Votes_for_resolution::find($id);
        if ($votes_for_resolution != null){
            $votes_for_resolution->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Votos de resolucion actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Votos de resolucion no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $votes_for_resolution = Votes_for_resolution::find($id);
        if ($votes_for_resolution != null){
            $votes_for_resolution->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Votos de resolucion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Votos de resolucion no encontrado",
            ]);
        }
    }
}
