<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;

use \stdClass;
use App\Models\Tool;
use App\Models\Block;
use App\Models\Measure;
use App\Models\Question;
use App\Models\Sub_questions;
use App\Models\Answer_type;

use App\Models\Removed_tool;
use App\Models\Removed_block;
use App\Models\Removed_measure;
use App\Models\Removed_question;
use App\Models\Removed_answer_type;

use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function index()
    {
        $tool = Tool::all();
        if ($tool != '[]'){
            return json_encode($tool);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen herramientas',
            ]);
        }
        
    }
 
    public function getAllDataTool( $id){
        $tool = Tool::where('id', 'like' , $id)->get();
        $array = array();
        $toolArray = array();
        $blocks = array();
        $blockArray = array();
        $measures = array();
        $measureArray = array();
        $questions = array();
        $questionArray = array();
        $sub_questions = array();
        $sub_questionArray = array();

        if ($tool != '[]'){
            $toolArray = (array)json_decode($tool);
            foreach ($toolArray as $tooldata) {
                $dataTool = (array)$tooldata;
                $block = Block::where('tool_id', 'like' , $id)->orderBy('position')->get();
                if ($block != '[]'){
                    foreach ($block as $datosBlock) {
                        $blockArray = (array)json_decode($datosBlock);
                        $measure = Measure::where('block_id', 'like' , $datosBlock['id'])->orderBy('position')->get();
                        if ($measure != '[]'){
                            foreach ($measure as $datosMeasure) {
                                $measureArray = (array)json_decode($datosMeasure);
                                $questiom = Question::where('measure_id', 'like' , $datosMeasure['id'])->orderBy('position')->get();
                                if ($questiom != '[]'){
                                    foreach ($questiom as $datosQuestion) {
                                        $questionArray = (array)json_decode($datosQuestion);
                                        $sub_question = Sub_questions::where('questions_id', 'like' , $datosQuestion['id'])->orderBy('position')->get();
                                        if ($sub_question != '[]'){
                                            foreach ($sub_question as $datosSubQuestion) {
                                                $sub_questionArray = (array)json_decode($datosSubQuestion);
                                                array_push($sub_questions,$sub_questionArray);
                                            }
                                            array_push($questionArray,$sub_questions);
                                        }
                                        array_push($questions,$questionArray);
                                    }
                                    array_push($measureArray, $questions);
                                }
                                array_push($measures, $measureArray);
                            }
                            array_push($blockArray, $measures);
                        }
                        array_push($blocks, $blockArray);
                        $blockArray = array();
                    }
                }
                array_push($dataTool, $blocks);
                $blocks = array();
            }
            array_push($array, $dataTool);
            return $array;  
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe tool',
            ]);
        }
    }
    
    public function store(Request $request)
    {
        $tool = new Tool();
        $tool->user_id = $request->input ('user_id');
        $tool->name = $request->input ('name');
        $tool->description = $request->input ('description');
        $tool->url = $request->input ('url');
        $tool->save();
        return response()->json([
            'status_code' => 200,
            'idTool' => $tool->id,
            'nameTool' => $tool->name,
            'url_image' => $tool->url,
        ]);
    }

    public function show($id)
    {
        $tool = Tool::find($id);
        if ($tool != null){
            return json_encode($tool);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe herramienta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $tool = Tool::find($id);
        if ($tool != null){
            $tool->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Heramienta actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Heramienta no encontrado",
            ]);
        }        
    }

    public function destroy($id)
    {
        $tool = Tool::find($id);
        if ($tool != null){
            $toolRemoved = new Removed_tool ();
            $toolRemoved->id = $id;
            $toolRemoved->user_id = $tool['user_id'];
            $toolRemoved->name = $tool['name'];
            $toolRemoved->description = $tool['description'];
            $toolRemoved->save();
            $block = Block::where('tool_id', 'like' , $id)->get();
            foreach ($block as $datosBlock) {
                $blockRemoved = new Removed_block ();
                $blockRemoved->id = $datosBlock['id'];
                $blockRemoved->removed_tool_id = $datosBlock['tool_id'];
                $blockRemoved->name = $datosBlock['name'];
                $blockRemoved->porcentage = $datosBlock['porcentage'];
                $blockRemoved->description = $datosBlock['description'];
                $blockRemoved->position = $datosBlock['position'];
                $blockRemoved->save();
                $measure = Measure::where('block_id', 'like' , $datosBlock['id'])->get();
                foreach ($measure as $datosMeasure) {
                    $measureRemoved = new Removed_measure();
                    $measureRemoved->id = $datosMeasure['id'];
                    $measureRemoved->removed_block_id = $datosMeasure['block_id'];
                    $measureRemoved->name = $datosMeasure['name'];
                    $measureRemoved->description = $datosMeasure['description'];
                    $measureRemoved->position = $datosMeasure['position'];
                    $measureRemoved->porcentage = $datosMeasure['porcentage'];
                    $measureRemoved->save();
                    $question = Question::where('measure_id', 'like' , $datosMeasure['id'])->get();
                    foreach ($question as $datosQuestion) {
                        $questionRemoved = new Removed_question();
                        $questionRemoved->id = $datosQuestion['id'];
                        $questionRemoved->removed_measure_id = $datosQuestion['measure_id'];
                        $questionRemoved->removed_answer_type_id = $datosQuestion['answer_type_id'];
                        $questionRemoved->name = $datosQuestion['name'];
                        $questionRemoved->description = $datosQuestion['description'];
                        $questionRemoved->position = $datosQuestion['position'];
                        $questionRemoved->save();
                    }
                }
            }
            $tool->delete();
            return response()->json([
                'status_code' => 200,
                'message' => "Herramienta eliminada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Herramienta no encontrada",
            ]);
        }
    }
}
