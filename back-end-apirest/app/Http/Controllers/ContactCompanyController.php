<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Contact_company;
use Illuminate\Http\Request;

class ContactCompanyController extends Controller
{
    public function index()
    {
        $contact_company = Contact_company::all();
        if ($contact_company != '[]'){
            return json_encode($contact_company);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Contacto de empresa',
            ]);
        }
    }

    public function store(Request $request)
    {
        $contact_company = new Contact_company();
        $contact_company->kind_of_society = $request->input ('kind_of_society');
        $contact_company->business_name = $request->input ('business_name');
        $contact_company->trade_registration = $request->input ('trade_registration');
        $contact_company->nit = $request->input ('nit');
        $contact_company->id_tool = $request->input ('id_tool');
        $contact_company->kind_of_society = $request->input ('kind_of_society');
        $contact_company->type_of_item = $request->input ('type_of_item');
        $contact_company->date = $request->input ('date');
        $contact_company->legal_address = $request->input ('legal_address');
        $contact_company->direction = $request->input ('direction');
        $contact_company->contact_person = $request->input ('contact_person');
        $contact_company->contact_number = $request->input ('contact_number');
        $contact_company->contact_email = $request->input ('contact_email');
        $contact_company->url_logo = $request->input ('url_logo');
        $contact_company->save();
    
        return response()->json([
            'status_code' => 200,
            'idcompany' => $contact_company->id,
            'business_name' => $contact_company->business_name,
        ]);
    }

    public function show($id)
    {
        $contact_company = Contact_company::find($id);
        if ($contact_company != null){
            return json_encode($contact_company);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Empresa',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $contact_company = Contact_company::find($id);
        if ($contact_company != null){
            $contact_company->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Contacto de empresa actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Contacto de empresa no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $contact_company = Contact_company::find($id);
        if ($contact_company != null){
            $id_contacto_empresa = $contact_company['id'];
            $contact_company->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "contacto empresa eliminado correctamente",
                'id_contacto_empresa' => $id_contacto_empresa
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "contacto empresa no encontrado",
            ]);
        }
    }
}
