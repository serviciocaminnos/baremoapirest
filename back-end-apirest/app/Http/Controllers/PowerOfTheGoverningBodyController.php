<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Power_of_the_governing_body;
use Illuminate\Http\Request;

class PowerOfTheGoverningBodyController extends Controller
{
    public function index()
    {
        $power_of_the_governing_body = Power_of_the_governing_body::all();
        if ($power_of_the_governing_body != '[]'){
            return json_encode($power_of_the_governing_body);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Competencias del organo de gobierno',
            ]);
        }
    }

    public function getPowerOfTheGoverningBody ($id_company){
        $power_of_the_governing_body = Power_of_the_governing_body::where('contact_id', 'like' , $id_company)->get();
        if ($power_of_the_governing_body != '[]'){
            return json_encode($power_of_the_governing_body);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen competencias de organo de gobierno',
            ]);
        }
    }

    public function store(Request $request)
    {
        $power_of_the_governing_body = new Power_of_the_governing_body();
        $power_of_the_governing_body->contact_id = $request->input ('contact_id');
        $power_of_the_governing_body->ordinary = $request->input ('ordinary');
        $power_of_the_governing_body->extraordinary = $request->input ('extraordinary');
        $power_of_the_governing_body->save();
        return response()->json([
            'status_code' => 200,
            'power_of_the_governing_body_id' => $power_of_the_governing_body->id,
        ]);
    }

    public function show($id)
    {
        $power_of_the_governing_body = Power_of_the_governing_body::find($id);
        if ($power_of_the_governing_body != null){
            return json_encode($power_of_the_governing_body);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe competencias de organo de gobierno',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $power_of_the_governing_body = Power_of_the_governing_body::find($id);
        if ($power_of_the_governing_body != null){
            $power_of_the_governing_body->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "competencias de organo de gobierno actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "competencias de organo de gobierno no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $power_of_the_governing_body = Power_of_the_governing_body::find($id);
        if ($power_of_the_governing_body != null){
            $power_of_the_governing_body->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "competencias de organo de gobierno eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "competencias de organo de gobierno no encontrado",
            ]);
        }
    }
}
