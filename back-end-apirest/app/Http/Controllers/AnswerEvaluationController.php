<?php

namespace App\Http\Controllers;

use App\Models\Answer_evaluation;
use Illuminate\Http\Request;

class AnswerEvaluationController extends Controller
{
    public function index()
    {
        $answer_evaluation = Answer_evaluation::all();
        if ($answer_evaluation != '[]'){
            return json_encode($answer_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen sub respuestas',
            ]);
        }
    }

    public function store(Request $request)
    {
        $answer_evaluation = new Answer_evaluation();
        $answer_evaluation->id = $request->input ('id_answer');
        $answer_evaluation->id_question_evaluation = $request->input ('id_question_evaluation');
        $answer_evaluation->id_subquestion_evaluation = $request->input ('id_subquestion_evaluation');
        $answer_evaluation->id_answer_type = $request->input ('id_answer_type');
        $answer_evaluation->id_answer = $request->input ('id_answer');
        $answer_evaluation->porcentage = $request->input ('porcentage');
        $answer_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $answer_evaluation->id,
            'id_question_evaluation' => $answer_evaluation->id_question_evaluation,
        ]);
    }

    public function show( $id)
    {
        $answer_evaluation = Answer_evaluation::find($id);
        if ($answer_evaluation != '[]' && $answer_evaluation != null){
            return json_encode($answer_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe respuesta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $answer_evaluation = Answer_evaluation::find($id);
        if ($answer_evaluation != null){
            $answer_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "respuesta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $answer_evaluation = Answer_evaluation::find($id);
        if ($answer_evaluation != null){
            $answer_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "respuesta en evaluacion no encontrado",
            ]);
        }
    }
}
