<?php

namespace App\Http\Controllers;

use App\Models\User_permits;
use Illuminate\Http\Request;

class UserPermitsController extends Controller
{
    public function index()
    {
        $user_permits = User_permits::all();
        if ($user_permits != '[]'){
            return json_encode($user_permits);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen permisos de usuario',
            ]);
        }
    }

    public function store(Request $request)
    {
        $user_permits = new User_permits();
        $user_permits->id_user = $request->input ('id_user');
        $user_permits->id_tool = $request->input ('id_tool');
        $user_permits->save();
        return response()->json([
            'status_code' => 200,
            'id' => $user_permits->id,
        ]);
    }

    public function show( $id)
    {
        $user_permits = User_permits::find($id);
        if ($user_permits != '[]' && $user_permits != null){
            return $user_permits;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe permisos de usuario',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $user_permits = User_permits::find($id);
        if ($user_permits != null){
            $user_permits->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "permisos de usuario actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "permisos de usuario no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $user_permits = User_permits::find($id);
        if ($user_permits != null){
            $user_permits->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "permisos de usuario eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "permisos de usuario no encontrado",
            ]);
        }
    }
}
