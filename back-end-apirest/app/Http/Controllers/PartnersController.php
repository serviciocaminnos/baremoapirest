<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Partners;
use App\Models\Participation_percentage;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index()
    {
        $partners = Partners::all();
        if ($partners != '[]'){
            return json_encode($partners);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen socios',
            ]);
        }
    }

    public function getPartnersContact ($id_company){
        $partners = Partners::where('contact_id', 'like' , $id_company)->get();
        $arrayPartners = array();
        $arrayDataPartners = array();
        if ($partners != '[]'){
            foreach ($partners as $dataPartners){
                $arrayDataPartners = (array)json_decode($dataPartners);
                $partisipacionPercentage = Participation_percentage::where('partners_id', 'like' , $dataPartners->id)->get();
                array_push($arrayDataPartners,$partisipacionPercentage);
                array_push($arrayPartners,$arrayDataPartners);
            }
            return $arrayPartners;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen datos extra',
            ]);
        }
    }

    public function store(Request $request)
    {
        $partners = new Partners();
        $partners->contact_id = $request->input ('contact_id');
        $partners->name = $request->input ('name');
        $partners->identification_document = $request->input ('identification_document');
        $partners->issued_in = $request->input ('issued_in');
        $partners->attorney = $request->input ('attorney');
        $partners->instrument_power = $request->input ('instrument_power');
        $partners->save();
        return response()->json([
            'status_code' => 200,
            'idcompany' => $partners->id,
            'name' => $partners->name,
        ]);
    }

    public function show($id)
    {
        $partners = Partners::find($id);
        if ($partners != null){
            return json_encode($partners);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe socios',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $partners = Partners::find($id);
        if ($partners != null){
            $partners->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "socio actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "socio no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $partners = Partners::find($id);
        if ($partners != null){
            $partners->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "socio eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "socio no encontrado",
            ]);
        }
    }
}
