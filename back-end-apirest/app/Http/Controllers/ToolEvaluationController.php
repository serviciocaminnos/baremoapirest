<?php

namespace App\Http\Controllers;

use App\Models\Tool_evaluation;
use Illuminate\Http\Request;

class ToolEvaluationController extends Controller
{
    public function index()
    {
        $tool_evaluation = Tool_evaluation::all();
        if ($tool_evaluation != '[]'){
            return json_encode($tool_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen herramienta evaluada',
            ]);
        }
    }

    public function store(Request $request)
    {
        $tool_evaluation = new Tool_evaluation();
        $tool_evaluation->id = $request->input ('id_tool');
        $tool_evaluation->id_evavaluation = $request->input ('id_evavaluation');
        $tool_evaluation->id_tool = $request->input ('id_tool');
        $tool_evaluation->porcentage = $request->input ('porcentage');
        $tool_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $tool_evaluation->id,
            'company_id' => $tool_evaluation->id_evavaluation,
        ]);
    }

    public function show( $id)
    {
        $tool_evaluation = Tool_evaluation::find($id);
        if ($tool_evaluation != '[]' && $tool_evaluation != null){
            return json_encode($tool_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe herramienta en evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $tool_evaluation = Tool_evaluation::find($id);
        if ($tool_evaluation != null){
            $tool_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Herramienta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $tool_evaluation = Tool_evaluation::find($id);
        if ($tool_evaluation != null){
            $tool_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Herramienta en evaluacion no encontrado",
            ]);
        }
    }
}
