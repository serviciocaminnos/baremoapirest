<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use Illuminate\Http\Request;

class EvaluationController extends Controller
{
    public function index()
    {
        $evaluation = evaluation::all();
        if ($evaluation != '[]'){
            return json_encode($evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen evaluacion',
            ]);
        }
    }

    public function store(Request $request)
    {
        $evaluation = new evaluation();
        $evaluation->company_id = $request->input ('company_id');
        $evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $evaluation->id,
            'company_id' => $evaluation->company_id,
        ]);
    }

    public function show( $id)
    {
        $evaluation = evaluation::find($id);
        if ($evaluation != '[]' && $evaluation != null){
            return json_encode($evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $evaluation = evaluation::find($id);
        if ($evaluation != null){
            $evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $evaluation = evaluation::find($id);
        if ($evaluation != null){
            $evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Evaluacion no encontrado",
            ]);
        }
    }
}
