<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Capital_contribution_type;
use Illuminate\Http\Request;

class CapitalContributionTypeController extends Controller
{
    public function index()
    {
        $capital_contribution_type = Capital_contribution_type::all();
        if ($capital_contribution_type != '[]'){
            return json_encode($capital_contribution_type);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen tipo de contribucion',
            ]);
        }
    }

    public function store(Request $request)
    {
        $capital_contribution_type = new Capital_contribution_type();
        $capital_contribution_type->social_capitals_id = $request->input ('social_capitals_id');
        $capital_contribution_type->cash = $request->input ('cash');
        $capital_contribution_type->equipment = $request->input ('equipment');
        $capital_contribution_type->moviliario = $request->input ('moviliario');
        $capital_contribution_type->save();
        return response()->json([
            'status_code' => 200,
            'capital_contribution_type_id' => $capital_contribution_type->id,
            'social_capitals_id' => $capital_contribution_type->social_capitals_id,
        ]);
    }

    public function show($id)
    {
        $capital_contribution_type = Capital_contribution_type::find($id);
        if ($capital_contribution_type != null){
            return json_encode($capital_contribution_type);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe tipo de contribucion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $capital_contribution_type = Capital_contribution_type::find($id);
        if ($capital_contribution_type != null){
            $capital_contribution_type->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "tipo de contribucion actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "tipo de contribucion no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $capital_contribution_type = Capital_contribution_type::find($id);
        if ($capital_contribution_type != null){
            $id_contribution = $capital_contribution_type['id'];
            $capital_contribution_type->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "tipo de contribucion eliminado correctamente",
                'id_capital_contribution' => $id_contribution
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "tipo de contribucion no encontrado",
            ]);
        }
    }
}
