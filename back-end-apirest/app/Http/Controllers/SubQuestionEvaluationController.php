<?php

namespace App\Http\Controllers;

use App\Models\Sub_question_evaluation;
use Illuminate\Http\Request;

class SubQuestionEvaluationController extends Controller
{
    public function index()
    {
        $sub_question_evaluation = Sub_question_evaluation::all();
        if ($sub_question_evaluation != '[]'){
            return json_encode($sub_question_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen sub pregunta evaluada',
            ]);
        }
    }

    public function store(Request $request)
    {
        $sub_question_evaluation = new Sub_question_evaluation();
        $sub_question_evaluation->id = $request->input ('id_subquestion');
        $sub_question_evaluation->id_question_evaluation = $request->input ('id_question_evaluation');
        $sub_question_evaluation->id_subquestion = $request->input ('id_subquestion');
        $sub_question_evaluation->porcentage = $request->input ('porcentage');
        $sub_question_evaluation->commentary = $request->input ('commentary');
        $sub_question_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $sub_question_evaluation->id,
            'id_question_evaluation' => $sub_question_evaluation->id_question_evaluation,
        ]);
    }

    public function show( $id)
    {
        $sub_question_evaluation = Sub_question_evaluation::find($id);
        if ($sub_question_evaluation != '[]' && $sub_question_evaluation != null){
            return json_encode($sub_question_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe sub pregunta en evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sub_question_evaluation = Sub_question_evaluation::find($id);
        if ($sub_question_evaluation != null){
            $sub_question_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "sub pregunta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "sub pregunta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $sub_question_evaluation = Sub_question_evaluation::find($id);
        if ($sub_question_evaluation != null){
            $sub_question_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "sub pregunta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "sub pregunta en evaluacion no encontrado",
            ]);
        }
    }
}
