<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Measure;
use App\Models\Question;
use App\Models\Answer_typr;


use App\Models\Removed_block;
use App\Models\Removed_measure;
use App\Models\Removed_question;
use App\Models\Removed_answer_type;

use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function index()
    {
        $block = Block::all();
        if ($block != '[]'){
            return json_encode($block);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen bloques',
            ]);
        }
    }

    public function getBlockOfTool($id)
    {
        $block = Block::where('tool_id', 'like' , $id)->orderBy('position')->get();
        if ($block != '[]'){
            return json_encode($block);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe bloque en en dicha pocicion',
            ]);
        }
    }

    public function getBlockWithPosition($pos,$id_tool)
    {
        $block = Block::where('position', 'like' , $pos)->where('tool_id', 'like' , $id_tool)->get();
        if ($block != "[]"){
            foreach ($block as $dBlock){
                $id = $dBlock['id'];
                $name = $dBlock['name'];
                $description = $dBlock['description'];
                $position = $dBlock['position'];
                $tool_id = $dBlock['tool_id'];
            }
            return response()->json([
                'status_code' => 200,
                'id' => $id,
                'name' => $name,
                'description' => $description,
                'position' => $position,
                'tool_id' => $tool_id,
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensage' => 'bloque no encontrado en la herramienta',
            ]);
        }
    }

    public function store(Request $request)
    {
        $block = new Block();
        $block->tool_id = $request->input ('tool_id');
        $block->name = $request->input ('name');
        $block->porcentage = $request->input ('porcentage');
        $block->description = $request->input ('description');

        //obtener posicion de un bloque
        $position = Block::where('tool_id', 'like' , $request->input ('tool_id'))->count();
        $block->position = $position + 1;
        $block->save();
        return response()->json([
            'status_code' => 200,
            'id' => $block->id,
            'name' => $block->name,
            'description' => $block->description,
        ]);
    }

    public function show($id)
    {
        $block = Block::find($id);
        if ($block != '[]' && $block != null){
            return json_encode($block);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe bloque',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sumPorcentage = 0;
        $block = Block::find($id);
        if ($block != null){
            $blocks = Block::where('tool_id', 'like' , $block->tool_id)->orderBy('position')->get();
            foreach ($blocks as $datablock){
                $porcentage = $datablock['porcentage'];
                $sumPorcentage = $sumPorcentage + $porcentage;
            }
            $sumPorcentage = $sumPorcentage-$block['porcentage'];
            if($sumPorcentage + $request->input ('porcentage') <= 100){
                $block->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "Bloque actualizado correctamente",
                ]);
            }
            else {
                $request["porcentage"] = $block->porcentage;
                $block->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "El porcentage supera el porcentage asignado",
                ]);
            }
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Bloque no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $block = Block::find($id);
        if($block != null){
            $blockRemoved = new Removed_block ();
            $blockRemoved->id = $block['id'];
            $blockRemoved->removed_tool_id = $block['tool_id'];
            $blockRemoved->name = $block['name'];
            $blockRemoved->porcentage = $block['porcentage'];
            $blockRemoved->description = $block['description'];
            $blockRemoved->position = $block['position'];
            $blockRemoved->save();
            $measure = Measure::where('block_id', 'like' , $block['id'])->get();
            foreach ($measure as $datosMeasure) {
                $measureRemoved = new Removed_measure();
                $measureRemoved->id = $datosMeasure['id'];
                $measureRemoved->removed_block_id = $datosMeasure['block_id'];
                $measureRemoved->name = $datosMeasure['name'];
                $measureRemoved->description = $datosMeasure['description'];
                $measureRemoved->position = $datosMeasure['position'];
                $measureRemoved->porcentage = $datosMeasure['porcentage'];
                $measureRemoved->save();
                $question = Question::where('measure_id', 'like' , $datosMeasure['id'])->get();
                foreach ($question as $datosQuestion) {
                    $questionRemoved = new Removed_question();
                    $questionRemoved->id = $datosQuestion['id'];
                    $questionRemoved->removed_measure_id = $datosQuestion['measure_id'];
                    $questionRemoved->removed_answer_type_id = $datosQuestion['answer_type_id'];
                    $questionRemoved->name = $datosQuestion['name'];
                    $questionRemoved->description = $datosQuestion['description'];
                    $questionRemoved->position = $datosQuestion['position'];
                    $questionRemoved->save();
                }
            }

            $blockRemovedTool = $block['tool_id'];
            $block->delete();
            // Reordenar bloques de una herramienta
            $blocks = Block::where('tool_id', 'like' , $blockRemovedTool)->orderBy('position')->get();
            $i = 1;
            foreach ($blocks as $oneBlock){
                $requestObj = new Request(array('position' => $i));
                $this->update($requestObj,$oneBlock['id']);
                $i++;
            }

            return response()->json([
                'status_code' => 200,
                'mensaje' => "Bloque eliminado correctamente",
                'id_tool' => $blockRemovedTool,
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Bloque no encontrado",
            ]);
        }
    }
}
