<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Specific_limits;
use Illuminate\Http\Request;

class SpecificLimitsController extends Controller
{
    public function index()
    {
        $specific_limits = Specific_limits::all();
        if ($specific_limits != '[]'){
            return json_encode($specific_limits);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Limites especificos',
            ]);
        }
    }

    public function store(Request $request)
    {
        $specific_limits = new Specific_limits();
        $specific_limits->representative_id = $request->input ('representative_id');
        $specific_limits->type = $request->input ('type');
        $specific_limits->amount = $request->input ('amount');
        $specific_limits->compentent_body = $request->input ('compentent_body');
        $specific_limits->mechanism = $request->input ('mechanism');
        $specific_limits->save();
        return response()->json([
            'status_code' => 200,
            'specific_limits_id' => $specific_limits->id,
        ]);
    }

    public function show($id)
    {
        $specific_limits = Specific_limits::find($id);
        if ($specific_limits != null){
            return json_encode($specific_limits);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Limites especificos',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $specific_limits = Specific_limits::find($id);
        if ($specific_limits != null){
            $specific_limits->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Limites especificos actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Limites especificos no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $specific_limits = Specific_limits::find($id);
        if ($specific_limits != null){
            $specific_limits->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Limites especificos eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Limites especificos en evaluacion no encontrado",
            ]);
        }
    }
}
