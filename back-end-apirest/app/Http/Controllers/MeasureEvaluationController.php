<?php

namespace App\Http\Controllers;

use App\Models\Measure_evaluation;
use Illuminate\Http\Request;

class MeasureEvaluationController extends Controller
{
    public function index()
    {
        $measure_evaluation = Measure_evaluation::all();
        if ($measure_evaluation != '[]'){
            return json_encode($measure_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen herramienta evaluada',
            ]);
        }
    }

    public function store(Request $request)
    {
        $measure_evaluation = new Measure_evaluation();
        $measure_evaluation->id = $request->input ('id_measure');
        $measure_evaluation->id_block_evavaluation = $request->input ('id_block_evavaluation');
        $measure_evaluation->id_measure = $request->input ('id_measure');
        $measure_evaluation->porcentage = $request->input ('porcentage');
        $measure_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $measure_evaluation->id,
            'id_block_evavaluation' => $measure_evaluation->id_block_evavaluation,
        ]);
    }

    public function show( $id)
    {
        $measure_evaluation = Measure_evaluation::find($id);
        if ($measure_evaluation != '[]' && $measure_evaluation != null){
            return json_encode($measure_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe herramienta en evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $measure_evaluation = Measure_evaluation::find($id);
        if ($measure_evaluation != null){
            $measure_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Herramienta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $measure_evaluation = Measure_evaluation::find($id);
        if ($measure_evaluation != null){
            $measure_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Herramienta en evaluacion no encontrado",
            ]);
        }
    }
}
