<?php

namespace App\Http\Controllers;

use App\Models\Block_evaluation;
use Illuminate\Http\Request;

class BlockEvaluationController extends Controller
{
    public function index()
    {
        $block_evaluation = Block_evaluation::all();
        if ($block_evaluation != '[]'){
            return json_encode($block_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen herramienta evaluada',
            ]);
        }
    }

    public function store(Request $request)
    {
        $block_evaluation = new Block_evaluation();
        $block_evaluation->id = $request->input ('id_block');
        $block_evaluation->id_tool_evavaluation = $request->input ('id_tool_evavaluation');
        $block_evaluation->id_block = $request->input ('id_block');
        $block_evaluation->porcentage = $request->input ('porcentage');
        $block_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $block_evaluation->id,
            'company_id' => $block_evaluation->id_evavaluation,
        ]);
    }

    public function show( $id)
    {
        $block_evaluation = Block_evaluation::find($id);
        if ($block_evaluation != '[]' && $block_evaluation != null){
            return json_encode($block_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe herramienta en evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $block_evaluation = Block_evaluation::find($id);
        if ($block_evaluation != null){
            $block_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Herramienta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $block_evaluation = Block_evaluation::find($id);
        if ($block_evaluation != null){
            $block_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Herramienta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Herramienta en evaluacion no encontrado",
            ]);
        }
    }
}
