<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        if ($user != '[]'){
            return json_encode($user);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Usuarios',
            ]);
        }
    }

    public function show($id)
    {
        $user = User::find($id);
        return response()->json([
            'status_code' => 200,
            'id' => $user->id,
            'name' => $user->name,
            'lastname' => $user->lastname,
            'username' => $user->username,
            'position' => $user->position,
            'user_image'=> $user->user_image,
            'rol' => $user->rol,
            'email' => $user->email,
        ]);
    }


    //administracion
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user != null){
            $pass = $request['password'];
            $request['password'] = Hash::make($pass);
            $user->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Usuario actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Usuario no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if ($user != null){
            $user->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Usuario eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Usuario no encontrado",
            ]);
        }   
    }
}
