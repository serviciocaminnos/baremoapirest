<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Measure;
use App\Models\Question;
use App\Models\Answer_type;

use App\Models\Removed_measure;
use App\Models\Removed_question;
use App\Models\Removed_answer_type;

use Illuminate\Http\Request;

class MeasureController extends Controller
{
    public function index()
    {
        $measure = Measure::all();
        if ($measure != '[]'){
            return json_encode($measure);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Medidas',
            ]);
        }
    }

    public function getMeasureOfBlock($id)
    {
        $measure = Measure::where('block_id', 'like' , $id)->orderBy('position')->get();
        if ($measure != '[]' && $measure != null){
            return json_encode($measure);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Medidas en el bloque',
            ]);
        }
    }

    public function getMeasureWithPosition($pos, $id_block)
    {
        $measure = Measure::where('position', 'like' , $pos)->where('block_id', 'like' , $id_block)->get();
        if ($measure != "[]" && $measure != null){
            foreach ($measure as $dmeasure){
                $id = $dmeasure['id'];
                $name = $dmeasure['name'];
                $description = $dmeasure['description'];
                $position = $dmeasure['position'];
                $porcentage = $dmeasure['porcentage'];
                $block_id = $dmeasure['block_id'];
            }
            $block = Block::where('id', 'like' , $block_id)->get();
            foreach ($block as $dBlock){
                $position_Block = $dmeasure['position'];
            }
            return response()->json([
                'status_code' => 200,
                'id' => $id,
                'id_block' => $block_id,
                'name' => $name,
                'description' => $description,
                'position' => $position,
                'porcentage' => $porcentage,
                'position_block' => $position_Block,
            ]);

        }
        else {
           return response()->json([
            'status_code' => 400,
            'mensage' => 'Medida no encontrado en el bloque',
            ]);
        }
    }

    public function store(Request $request)
    {
        $measure = new Measure();
        $measure->block_id = $request->input ('block_id');
        $measure->name = $request->input ('name');
        $measure->description = $request->input ('description');

        //obtener posicion de un bloque
        $position = Measure::where('block_id', 'like' , $request->input ('block_id'))->count();
        $measure->position = $position + 1;
        $measure->porcentage = $request->input ('porcentage');
        $measure->save();
        return response()->json([
            'status_code' => 200,
            'id' => $measure->id,
            'name' => $measure->name,
            'description' => $measure->description,
            'position' => $measure->position,
        ]);
    }

    public function show($id)
    {
        $measure = Measure::find($id);
        if ($measure != '[]' && $measure != null){
            return json_encode($measure);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Medida',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sumPorcentage = 0;
        $measure = Measure::find($id);
        if ($measure != null){
            $measures = Measure::where('block_id', 'like' , $measure->block_id)->orderBy('position')->get();
            foreach ($measures as $dataMeasure){
                $porcentage = $dataMeasure['porcentage'];
                $sumPorcentage = $sumPorcentage + $porcentage;
            }
            $block = Block::find($measure->block_id);
            $sumPorcentage = $sumPorcentage - $measure['porcentage'];
            if($sumPorcentage + $request->input ('porcentage') <= $block['porcentage']){
                $measure->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensage' => "Medida actualizada correctamente",
                ]);
            }
            else {
                $request["porcentage"] = $measure->porcentage;
                $measure->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "El porcentage supera el porcentage asignado",
                ]);
            }    
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensage' => "Medida no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $measure = Measure::find($id);
        if ($measure != null){
            $measureRemoved = new Removed_measure();
            $measureRemoved->id = $measure['id'];
            $measureRemoved->removed_block_id = $measure['block_id'];
            $measureRemoved->name = $measure['name'];
            $measureRemoved->description = $measure['description'];
            $measureRemoved->position = $measure['position'];
            $measureRemoved->porcentage = $measure['porcentage'];
            $measureRemoved->save();
            $question = Question::where('measure_id', 'like' , $measure['id'])->get();
            foreach ($question as $datosQuestion) {
                $questionRemoved = new Removed_question();
                $questionRemoved->id = $datosQuestion['id'];
                $questionRemoved->removed_measure_id = $datosQuestion['measure_id'];
                $questionRemoved->removed_answer_type_id = $datosQuestion['answer_type_id'];
                $questionRemoved->name = $datosQuestion['name'];
                $questionRemoved->description = $datosQuestion['description'];
                $questionRemoved->position = $datosQuestion['position'];
                $questionRemoved->save();
            }

            $measureRemovedBlock = $measure['block_id'];

            $measure->delete();

            // Reordenar medidas de una de un blocke
            $measures = Measure::where('block_id', 'like' , $measureRemovedBlock)->orderBy('position')->get();
            $i = 1;
            foreach ($measures as $oneMeasures){
                $requestObj = new Request(array('position' => $i));
                $this->update($requestObj,$oneMeasures['id']);
                $i++;
            }
            return response()->json([
                'status_code' => 200,
                'mensage' => "Medida eliminada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Medida no encontrado",
            ]);
        }
    }
}
