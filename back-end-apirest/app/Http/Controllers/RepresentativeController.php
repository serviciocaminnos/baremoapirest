<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Representative;
use App\Models\Specific_limits;
use Illuminate\Http\Request;

class RepresentativeController extends Controller
{
    public function index()
    {
        $representative = Representative::all();
        if ($representative != '[]'){
            return json_encode($representative);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Representante',
            ]);
        }
    }

    public function getRepresentative ($id_company){
        $representative = Representative::where('contact_id', 'like' , $id_company)->get();
        $representante = array();
        $limitesEspecificos = array();
        if ($representative != '[]'){
            foreach ($representative as $repre) {
                $specific_limits = Specific_limits::where('representative_id', 'like' , $repre['id'])->get();              
                $representante = (array)json_decode($repre);
                $limitesEspecificos = (array)json_decode($specific_limits);
                array_push($representante, $limitesEspecificos);
                return $representante;
            }
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen convocatoria a asamblea de socios',
            ]);
        }
    }

    public function store(Request $request)
    {
        $representative = new Representative();
        $representative->contact_id = $request->input ('contact_id');
        $representative->representative_name = $request->input ('representative_name');
        $representative->identification_document = $request->input ('identification_document');
        $representative->issued_in = $request->input ('issued_in');
        $representative->denomination_of_the_position = $request->input ('denomination_of_the_position');
        $representative->save();
        return response()->json([
            'status_code' => 200,
            'representative_id' => $representative->id,
        ]);
    }

    public function show($id)
    {
        $representative = Representative::find($id);
        if ($representative != null){
            return json_encode($representative);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Representante',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $representative = Representative::find($id);
        if ($representative != null){
            $representative->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Representante actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Representante no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $representative = Representative::find($id);
        if ($representative != null){
            $representative->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Representante eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Representante no encontrado",
            ]);
        }
    }
}
