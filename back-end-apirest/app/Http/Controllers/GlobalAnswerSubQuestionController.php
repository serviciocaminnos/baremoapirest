<?php

namespace App\Http\Controllers;

use App\Models\Sub_questions;
use App\Models\Global_answer_sub_question;
use Illuminate\Http\Request;

class GlobalAnswerSubQuestionController extends Controller
{
    public function index()
    {
        $global_answer_sub_question = Global_answer_sub_question::all();
        if ($global_answer_sub_question != '[]'){
            return json_encode($global_answer_sub_question);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function getAnswerBySubQuestion($id)
    {
        $answers = Global_answer_sub_question::where('id_sub_question', 'like' , $id)->get();
        if ($answers != '[]' && $answers != null){
            return json_encode($answers);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function store(Request $request)
    {
        $global_answer_sub_question = new Global_answer_sub_question();
        $global_answer_sub_question->id_sub_question_conf = $request->input ('id_sub_question_conf');
        $global_answer_sub_question->id_sub_question = $request->input ('id_sub_question');
        $global_answer_sub_question->name = $request->input ('name');
        $global_answer_sub_question->id_type_answer = $request->input ('id_type_answer');
        $global_answer_sub_question->id_answer = $request->input ('id_answer');
        $global_answer_sub_question->porcentage = $request->input ('porcentage');
        $global_answer_sub_question->sub_answer = $request->input ('sub_answer');
        $global_answer_sub_question->need = $request->input ('need');
        $global_answer_sub_question->save();
        return response()->json([
            'status_code' => 200,
            'id' => $global_answer_sub_question->id,
        ]);
    }

    public function show( $id)
    {
        $global_answer_sub_question = Global_answer_sub_question::find($id);
        if ($global_answer_sub_question != '[]' && $global_answer_sub_question != null){
            return $global_answer_sub_question;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe respuesta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sumPorcentage = 0;
        $global_answer_sub_question = Global_answer_sub_question::find($id);
        if ($global_answer_sub_question != null){
            $global_answer_sub_questions = Global_answer_sub_question::where('id_sub_question', 'like' , $global_answer_sub_question->id_sub_question)->get();
            foreach ($global_answer_sub_questions as $dataGlobal_answer_sub_question){
                $porcentage = $dataGlobal_answer_sub_question['porcentage'];
                $sumPorcentage = $sumPorcentage + $porcentage;
            }
            $sub_questions = Sub_questions::find($global_answer_sub_question->id_sub_question);
            $sumPorcentage = $sumPorcentage - $global_answer_sub_question['porcentage'];
            if($sumPorcentage + $request->input ('porcentage') <= $sub_questions['porcentage']){
                $global_answer_sub_question->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "Respuesta global actualizado correctamente",
                ]);
            }
            else {
                $request["porcentage"] = $global_answer_sub_question->porcentage;
                $global_answer_sub_question->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "El porcentage supera el porcentage asignado",
                ]);
            }
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "Respuesta global no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $global_answer_sub_question = Global_answer_sub_question::find($id);
        if ($global_answer_sub_question != null){
            $global_answer_sub_question->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Respuesta global eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Respuesta global no encontrado",
            ]);
        }
    }
}
