<?php

namespace App\Http\Controllers;

use App\Models\GlobalAnswerSubQuestionEvaluation;
use Illuminate\Http\Request;

class GlobalAnswerSubQuestionEvaluationController extends Controller
{
    public function index()
    {
        $globalAnswerSubQuestionEvaluation = GlobalAnswerSubQuestionEvaluation::all();
        if ($globalAnswerSubQuestionEvaluation != '[]'){
            return json_encode($globalAnswerSubQuestionEvaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function getAnswerBySubQuestion($id)
    {
        $answers = GlobalAnswerSubQuestionEvaluation::where('id_sub_question', 'like' , $id)->get();
        if ($answers != '[]' && $answers != null){
            return json_encode($answers);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function store(Request $request)
    {
        $globalAnswerSubQuestionEvaluation = new GlobalAnswerSubQuestionEvaluation();
        $globalAnswerSubQuestionEvaluation->id_subquestioneva = $request->input ('id_subquestioneva');
        $globalAnswerSubQuestionEvaluation->id_sub_question = $request->input ('id_sub_question');
        $globalAnswerSubQuestionEvaluation->name = $request->input ('name');
        $globalAnswerSubQuestionEvaluation->id_type_answer = $request->input ('id_type_answer');
        $globalAnswerSubQuestionEvaluation->id_answer = $request->input ('id_answer');
        $globalAnswerSubQuestionEvaluation->porcentage = $request->input ('porcentage');
        $globalAnswerSubQuestionEvaluation->sub_answer = $request->input ('sub_answer');
        $globalAnswerSubQuestionEvaluation->need = $request->input ('need');
        $globalAnswerSubQuestionEvaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $globalAnswerSubQuestionEvaluation->id,
        ]);
    }

    public function show( $id)
    {
        $globalAnswerSubQuestionEvaluation = GlobalAnswerSubQuestionEvaluation::find($id);
        if ($globalAnswerSubQuestionEvaluation != '[]' && $globalAnswerSubQuestionEvaluation != null){
            return $globalAnswerSubQuestionEvaluation;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe respuesta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $globalAnswerSubQuestionEvaluation = GlobalAnswerSubQuestionEvaluation::find($id);
        if ($globalAnswerSubQuestionEvaluation != null){
            $globalAnswerSubQuestionEvaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "respuesta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $globalAnswerSubQuestionEvaluation = GlobalAnswerSubQuestionEvaluation::find($id);
        if ($globalAnswerSubQuestionEvaluation != null){
            $globalAnswerSubQuestionEvaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "respuesta en evaluacion no encontrado",
            ]);
        }
    }
}
