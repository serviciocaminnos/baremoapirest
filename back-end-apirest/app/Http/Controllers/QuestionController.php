<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Measure;
use App\Models\Question;
use App\Models\Answer_type;
use App\Models\Answer;

use App\Models\Removed_question;
use App\Models\Removed_answer_type;

use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        $question = Question::all();
        if ($question != '[]'){
            return json_encode($question);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Preguntas',
            ]);
        }
    }

    public function getQuestionOfMeasure($id)
    {
        $question = Question::where('measure_id', 'like' , $id)->orderBy('position')->get();
        if ($question != '[]'){
            return json_encode($question);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Pregunta',
            ]);
        }
    }

    public function getQuiestionWithPosition($pos, $id_measure)
    {
        $question = Question::where('position', 'like' , $pos)->where('measure_id', 'like' , $id_measure)->get();
        if ($question != "[]"){
            foreach ($question as $dQuestion){
                $id = $dQuestion['id'];
                $name = $dQuestion['name'];
                $description = $dQuestion['description'];
                $position = $dQuestion['position'];
                $measure_id = $dQuestion['measure_id'];
                $answer_type_id = $dQuestion['answer_type_id'];
            }
            $measure = Measure::where('id', 'like' , $measure_id)->get();
            foreach ($measure as $dMeasure){
                $block_id = $dMeasure['block_id'];
                $position_meassure = $dMeasure['position'];
            }
            $block = Block::where('id', 'like' , $block_id)->get();
            foreach ($block as $dblock){
                $position_block = $dblock['position'];
            }
            $answer = null;
            if ($answer_type_id != null){
                $answer = Answer::where('answer_type_id', 'like' , $answer_type_id)->get();
            } 
            $datosQues = [
                'status_code' => 200,
                'id' => $id,
                'answer_type_id' => $answer_type_id,
                'id_measure' => $measure_id,
                'id_block' => $block_id,
                'name' => $name,
                'description' => $description,
                'position' => $position,
                'position_measure' => $position_meassure,
                'position_block' => $position_block,
                'tipo' =>$answer
            ];

            $json_data = json_encode($datosQues);
            return $json_data;
        }
        else {
           return response()->json([
            'status_code' => 400,
            'mensage' => 'preguna no encontrado en la medida',
            ]);
        }
    }

    public function store(Request $request)
    {
        $question = new Question();
        $question->measure_id = $request->input ('measure_id');
        $question->answer_type_id = $request->input ('answer_type_id');
        $question->name = $request->input ('name');
        $question->description = $request->input ('description');
        $question->porcentage = $request->input ('porcentage');

        //obtener posicion de un bloque
        $position = Question::where('measure_id', 'like' , $request->input ('measure_id'))->count();
        $question->position = $position + 1;
        $question->save();
        return response()->json([
            'status_code' => 200,
            'id' => $question->id,
            'name' => $question->name,
            'description' => $question->description,
        ]);
    }

    public function show($id)
    {
        $question = Question::find($id);
        if ($question != '[]' && $question != null){
            return json_encode($question);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Pregunta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sumPorcentage = 0;
        $question = Question::find($id);
        if ($question != null){
            $questions = Question::where('measure_id', 'like' , $question->measure_id)->orderBy('position')->get();
            foreach ($questions as $dataQuestions){
                $porcentage = $dataQuestions['porcentage'];
                $sumPorcentage = $sumPorcentage + $porcentage;
            }
            $measure = Measure::find($question->measure_id);
            $sumPorcentage = $sumPorcentage - $question['porcentage'];
            if($sumPorcentage + $request->input ('porcentage') <= $measure['porcentage']){
                $question->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensage' => "Pregunta actualizada correctamente",
                ]);
            }
            else {
                $request["porcentage"] = $question->porcentage;
                $question->update($request->all());
                return response()->json([
                    'status_code' => 200,
                    'mensaje' => "El porcentage supera el porcentage asignado",
                ]);
            }    
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Pregunta no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $question = Question::find($id);
        if ($question != null){
            $questionRemoved = new Removed_question();
            $questionRemoved->id = $question['id'];
            $questionRemoved->removed_measure_id = $question['measure_id'];
            $questionRemoved->removed_answer_type_id = $question['answer_type_id'];
            $questionRemoved->name = $question['name'];
            $questionRemoved->description = $question['description'];
            $questionRemoved->position = $question['position'];
            $questionRemoved->save();

            $questionRemovedMeasure = $question['measure_id'];

            $question->delete();

            // Reordenar preguntas de una de una medida
            $questions = Question::where('measure_id', 'like' , $questionRemovedMeasure)->orderBy('position')->get();
            $i = 1;
            foreach ($questions as $oneQuestion){
                $requestObj = new Request(array('position' => $i));
                $this->update($requestObj,$oneQuestion['id']);
                $i++;
            }

            return response()->json([
                'status_code' => 200,
                'mensage' => "Pregunta eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Pregunta no encontrado",
            ]);
        }
    }
}
