<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Participation_percentage;
use Illuminate\Http\Request;

class ParticipationPercentageController extends Controller
{
    public function index()
    {
        $participation_percentage = Participation_percentage::all();
        if ($participation_percentage != '[]'){
            return json_encode($participation_percentage);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Participaciones de socios',
            ]);
        }
    }

    public function store(Request $request)
    {
        $participation_percentage = new Participation_percentage();
        $participation_percentage->partners_id = $request->input ('partners_id');
        $participation_percentage->social_capitals_id = $request->input ('social_capitals_id');
        $participation_percentage->input = $request->input ('input');
        $participation_percentage->capital_quota = $request->input ('capital_quota');
        $participation_percentage->participation = $request->input ('participation');
        $participation_percentage->save();
        return response()->json([
            'status_code' => 200,
            'participation_id' => $participation_percentage->id,
            'partners_id' => $participation_percentage->partners_id,
        ]);
    }

    public function show($id)
    {
        $participation_percentage = Participation_percentage::find($id);
        if ($participation_percentage != null){
            return json_encode($participation_percentage);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Participaciones',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $participation_percentage = Participation_percentage::find($id);
        if ($participation_percentage != null){
            $participation_percentage->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Participacion actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Participacion no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $participation_percentage = Participation_percentage::find($id);
        if ($participation_percentage != null){
            $participation_percentage->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Participacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Participacion no encontrado",
            ]);
        }
    }
}
