<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Social_capital;
use App\Models\Capital_contribution_type;
use Illuminate\Http\Request;

class SocialCapitalController extends Controller
{
    public function index()
    {
        $social_capital = social_capital::all();
        if ($social_capital != '[]'){
            return json_encode($social_capital);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Capital social',
            ]);
        }
    }

    public function getSocialCapitalContact ($id_company){
        $social_capital = Social_capital::where('Company_id', 'like' , $id_company)->get();
        $arraySocialCapital = array();
        $data = array();
        if ($social_capital != '[]'){
            foreach ($social_capital as $dataCapital){
                $data = (array)json_decode($dataCapital);
                $capital_contribution_type = Capital_contribution_type::where('social_capitals_id', 'like' , $dataCapital->id)->get();
                array_push($data,$capital_contribution_type);
                array_push($arraySocialCapital,$data);
            }
            return $arraySocialCapital;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen capital social de la emppresa',
            ]);
        }
    }

    public function store(Request $request)
    {
        $social_capital = new social_capital();
        $social_capital->Company_id = $request->input ('Company_id');
        $social_capital->capital = $request->input ('capital');
        $social_capital->exchange_rate = $request->input ('exchange_rate');
        $social_capital->save();
        return response()->json([
            'status_code' => 200,
            'participation_id' => $social_capital->id,
        ]);
    }

    public function show($id)
    {
        $social_capital = social_capital::find($id);
        if ($social_capital != null){
            return json_encode($social_capital);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Capital social',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $social_capital = social_capital::find($id);
        if ($social_capital != null){
            $social_capital->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Capital social actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Capital social no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $social_capital = Social_capital::find($id);
        if ($social_capital != null){
            $social_capital->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Capital social eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Capital social no encontrado",
            ]);
        }
    }
}
