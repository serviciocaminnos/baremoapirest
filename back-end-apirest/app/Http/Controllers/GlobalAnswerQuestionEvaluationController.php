<?php

namespace App\Http\Controllers;

use App\Models\GlobalAnswerQuestionEvaluation;
use Illuminate\Http\Request;

class GlobalAnswerQuestionEvaluationController extends Controller
{
    public function index()
    {
        $globalAnswerQuestionEvaluation = GlobalAnswerQuestionEvaluation::all();
        if ($globalAnswerQuestionEvaluation != '[]'){
            return json_encode($globalAnswerQuestionEvaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function getAnswerByQuestion($id)
    {
        $answers = GlobalAnswerQuestionEvaluation::where('id_question', 'like' , $id)->get();
        if ($answers != '[]' && $answers != null){
            return json_encode($answers);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function store(Request $request)
    {
        $globalAnswerQuestionEvaluation = new GlobalAnswerQuestionEvaluation();
        $globalAnswerQuestionEvaluation->id_questioneva = $request->input ('id_questioneva');
        $globalAnswerQuestionEvaluation->id_question = $request->input ('id_question');
        $globalAnswerQuestionEvaluation->name = $request->input ('name');
        $globalAnswerQuestionEvaluation->id_type_answer = $request->input ('id_type_answer');
        $globalAnswerQuestionEvaluation->id_answer = $request->input ('id_answer');
        $globalAnswerQuestionEvaluation->porcentage = $request->input ('porcentage');
        $globalAnswerQuestionEvaluation->sub_answer = $request->input ('sub_answer');
        $globalAnswerQuestionEvaluation->need = $request->input ('need');
        $globalAnswerQuestionEvaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $globalAnswerQuestionEvaluation->id,
        ]);
    }

    public function show( $id)
    {
        $globalAnswerQuestionEvaluation = GlobalAnswerQuestionEvaluation::find($id);
        if ($globalAnswerQuestionEvaluation != '[]' && $globalAnswerQuestionEvaluation != null){
            return $globalAnswerQuestionEvaluation;
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe respuesta',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $globalAnswerQuestionEvaluation = GlobalAnswerQuestionEvaluation::find($id);
        if ($globalAnswerQuestionEvaluation != null){
            $globalAnswerQuestionEvaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "respuesta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $globalAnswerQuestionEvaluation = GlobalAnswerQuestionEvaluation::find($id);
        if ($globalAnswerQuestionEvaluation != null){
            $globalAnswerQuestionEvaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "respuesta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "respuesta en evaluacion no encontrado",
            ]);
        }
    }
}
