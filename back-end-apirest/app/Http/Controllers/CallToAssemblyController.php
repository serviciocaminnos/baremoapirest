<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Call_to_assembly;
use Illuminate\Http\Request;

class CallToAssemblyController extends Controller
{
    public function index()
    {
        $call_to_assembly = Call_to_assembly::all();
        if ($call_to_assembly != '[]'){
            return json_encode($call_to_assembly);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Convocatoria',
            ]);
        }
    }
    
    public function getCallToAssembly ($id_company){
        $call_to_assembly = Call_to_assembly::where('contact_id', 'like' , $id_company)->get();
        if ($call_to_assembly != '[]'){
            return json_encode($call_to_assembly);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen convocatoria a asamblea de socios',
            ]);
        }
    }

    public function store(Request $request)
    {
        $call_to_assembly = new Call_to_assembly();
        $call_to_assembly->contact_id = $request->input ('contact_id');
        $call_to_assembly->medium = $request->input ('medium');
        $call_to_assembly->anticipation_period = $request->input ('anticipation_period');
        $call_to_assembly->save();
        return response()->json([
            'status_code' => 200,
            'call_to_assembly_id' => $call_to_assembly->id,
        ]);
    }

    public function show($id)
    {
        $call_to_assembly = Call_to_assembly::find($id);
        if ($call_to_assembly != null){
            return json_encode($call_to_assembly);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Convocatoria',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $call_to_assembly = Call_to_assembly::find($id);
        if ($call_to_assembly != null){
            $call_to_assembly->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Convocatoria actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Convocatoria no encontrado",
            ]);
        }
    }

    public function destroy(Call_to_assembly $call_to_assembly)
    {
        $call_to_assembly = Call_to_assembly::find($id);
        if ($call_to_assembly != null){
            $call_to_assembly->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "llamda a asamblea dee socios eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "llamda a asamblea dee socios en evaluacion no encontrado",
            ]);
        }
    }
}
