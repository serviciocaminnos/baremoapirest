<?php

namespace App\Http\Controllers;

use App\Models\Question_evaluation;
use Illuminate\Http\Request;

class QuestionEvaluationController extends Controller
{
    public function index()
    {
        $question_evaluation = Question_evaluation::all();
        if ($question_evaluation != '[]'){
            return json_encode($question_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen pregunta evaluada',
            ]);
        }
    }

    public function store(Request $request)
    {
        $question_evaluation = new Question_evaluation();
        $question_evaluation->id = $request->input ('id_question');
        $question_evaluation->id_measure_evavaluation = $request->input ('id_measure_evavaluation');
        $question_evaluation->id_question = $request->input ('id_question');
        $question_evaluation->porcentage = $request->input ('porcentage');
        $question_evaluation->commentary = $request->input ('commentary');
        $question_evaluation->save();
        return response()->json([
            'status_code' => 200,
            'id' => $question_evaluation->id,
            'id_measure_evavaluation' => $question_evaluation->id_measure_evavaluation,
        ]);
    }

    public function show( $id)
    {
        $question_evaluation = Question_evaluation::find($id);
        if ($question_evaluation != '[]' && $question_evaluation != null){
            return json_encode($question_evaluation);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe pregunta en evaluacion',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $question_evaluation = Question_evaluation::find($id);
        if ($question_evaluation != null){
            $question_evaluation->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensaje' => "pregunta en evaluacion actualizado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensaje' => "pregunta en evaluacion no encontrada",
            ]);
        }
    }

    public function destroy( $id)
    {
        $question_evaluation = Question_evaluation::find($id);
        if ($question_evaluation != null){
            $question_evaluation->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "pregunta en evaluacion eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "pregunta en evaluacion no encontrado",
            ]);
        }
    }
}
