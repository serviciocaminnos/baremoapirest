<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Answer_type;

use App\Models\Removed_answer;

use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function index()
    {
        $answer = Answer::all();
        if ($answer != '[]'){
            return json_encode($answer);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function getAnswerOfAnswerType($id)
    {
        $answer = Answer::where('answer_type_id', 'like' , $id)->orderBy('position')->get();
        if ($answer != '[]' && $answer != null){
            return json_encode($answer);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function getAnswerWithPosition($pos, $id_answer_type)
    {
        $answer = Answer::where('position', 'like' , $pos)->where('answer_type_id', 'like' , $id_answer_type)->get();
        if ($answer != "[]"){
            foreach ($answer as $dAnswer){
                $id = $dAnswer['id'];
                $answer_type_id = $dAnswer['answer_type_id'];
                $name = $dAnswer['name'];
                $position = $dAnswer['position'];
            }
            return response()->json([
                'status_code' => 200,
                'id' => $id,
                'answer_type_id' => $answer_type_id,
                'name' => $name,
                'position' => $position,
            ]);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'mensage' => 'respuesta no encontrado en el tipo de respuesta',
            ]);
        }
    }

    public function store(Request $request)
    {
        $answer = new Answer();
        $answer->answer_type_id = $request->input ('answer_type_id');
        $answer->name = $request->input ('name');

        $position = Answer::where('answer_type_id', 'like' , $request->input ('answer_type_id'))->count();
        $answer->position = $position + 1;
        $answer->porcentage = $request->input ('porcentage');
        $answer->save();
        return response()->json([
            'status_code' => 200,
            'id' => $answer->id,
            'name' => $answer->name,
        ]);
    }

    public function show($id)
    {
        $answer = Answer::find($id);
        if ($answer != '[]' && $answer != null){
            return json_encode($answer);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen respuestas',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $answer = Answer::find($id);
        if($answer != null){
            $answer->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensage' => "Respuesta actualizados correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Respuesta no encontrado",
            ]);
        }
    }

    public function destroy($id)
    {
        $answer = Answer::find($id);
        if ($answer != null){
            $answerRemoved = new Removed_answer();
            $answerRemoved->id = $answer['id'];
            $answerRemoved->removed_answer_type_id = $answer['answer_type_id'];
            $answerRemoved->name = $answer['name'];
            $answerRemoved->position = $answer['position'];
            $answerRemoved->porcentage = $answer['porcentage'];
            $answerRemoved->save();

            $answerRemovedtypeAnswer = $answer['answer_type_id'];

            $answer->delete();

            // Reordenar respuestas de una de un typo de respuesta
            $answer = Answer::where('answer_type_id', 'like' , $answerRemovedtypeAnswer)->orderBy('position')->get();
            $i = 1;
            foreach ($answer as $oneAnswer){
                $requestObj = new Request(array('position' => $i));
                $this->update($requestObj,$oneAnswer['id']);
                $i++;
            }
            return response()->json([
                'status_code' => 200,
                'mensage' => "Medida eliminada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Medida no encontrado",
            ]);
        }
    }
}
