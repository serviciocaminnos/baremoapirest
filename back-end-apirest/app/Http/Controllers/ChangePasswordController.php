<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function change_password(Request $request, $id)
    {
        $response = null;
        $input = $request->all();
        $user = User::find($id);
        $rules = array(
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) {
            $response = response()->json([
                'status_code' => 400,
                'message' =>  $validator->errors()->first(),
            ]);
        } 
        else {
            if (Hash::check($request['old_password'], $user['password']) == true) {  
                if (Hash::check($request['old_password'], $request['new_password']) == false){
                    User::where('id', $id)->update(['password' => Hash::make($input['new_password'])]);
                    $response = response()->json([
                        'status_code' => 200,
                        'message' => 'Password updated successfully.',
                    ]);
                }
                else {
                    $response = response()->json([
                        'status_code' => 400,
                        'message' => 'Please enter a password which is not similar then current password.',
                    ]);
                }
            } 
            else if (Hash::check($request['old_password'], $user['password']) == false) {
                $response = response()->json([
                    'status_code' => 400,
                    'message' => 'Check your old password.',
                ]);
            }
        }
        return $response;
    }   
}   