<?php

namespace App\Http\Controllers;

use App\Models\Sub_questions;
use Illuminate\Http\Request;

class SubQuestionsController extends Controller
{
    public function index()
    {
        $sub_questions = Sub_questions::all();
        if ($sub_questions != '[]'){
            return json_encode($sub_questions);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Sub preguntas',
            ]);
        }
    }

    public function store(Request $request)
    {
        $sub_questions = new Sub_questions();
        $sub_questions->questions_id = $request->input ('questions_id');
        $sub_questions->answer_type_id = $request->input ('answer_type_id');
        $sub_questions->name = $request->input ('name');
        $sub_questions->description = $request->input ('description');

        //obtener posicion de un bloque
        $position = Sub_questions::where('questions_id', 'like' , $request->input ('questions_id'))->count();
        $sub_questions->position = $position + 1;
        $sub_questions->porcentage = $request->input ('porcentage');
        $sub_questions->save();
        return response()->json([
            'status_code' => 200,
            'id' => $sub_questions->id,
            'name' => $sub_questions->name,
            'description' => $sub_questions->description,
        ]);
    }

    public function show( $id )
    {
        $sub_questions = Sub_questions::find($id);
        if ($sub_questions != '[]' && $sub_questions != null){
            return json_encode($sub_questions);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Sub preguntas',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $sub_questions = Sub_questions::find($id);
        if ($sub_questions != null){
            $sub_questions->update($request->all());
            return response()->json([
                'status_code' => 200,
                'mensage' => "Sub pregunta actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Sub pregunta no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $sub_questions = Sub_questions::find($id);
        if ($sub_questions != null){
            $sub_questions->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Sub pregunta eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Sub pregunta no encontrado",
            ]);
        }
    }
}
