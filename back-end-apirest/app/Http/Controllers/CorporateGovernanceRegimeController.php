<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Corporate_governance_regime;
use Illuminate\Http\Request;

class CorporateGovernanceRegimeController extends Controller
{
    public function index()
    {
        $corporate_governance_regime = Corporate_governance_regime::all();
        if ($corporate_governance_regime != '[]'){
            return json_encode($corporate_governance_regime);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Regimen de gobierno corporativo',
            ]);
        }
    }

    public function store(Request $request)
    {
        $corporate_governance_regime = new Corporate_governance_regime();
        $corporate_governance_regime->id_legal_reserves = $request->input ('id_legal_reserves');
        $corporate_governance_regime->ordinary = $request->input ('ordinary');
        $corporate_governance_regime->extraordinary = $request->input ('extraordinary');
        $corporate_governance_regime->save();
        return response()->json([
            'status_code' => 200,
            'corporate_governance_regime_id' => $corporate_governance_regime->id,
        ]);
    }

    public function show($id)
    {
        $corporate_governance_regime = Corporate_governance_regime::find($id);
        if ($corporate_governance_regime != null){
            return json_encode($corporate_governance_regime);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Regimen de gobierno corporativo',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $corporate_governance_regime = Corporate_governance_regime::find($id);
        if ($corporate_governance_regime != null){
            $corporate_governance_regime->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Regimen de gobierno corporativo actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Regimen de gobierno corporativo no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $corporate_governance_regime = Corporate_governance_regime::find($id);
        if ($corporate_governance_regime != null){
            $corporate_governance_regime->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Regimen de gobierno corporativo eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Regimen de gobierno corporativo no encontrado",
            ]);
        }
    }
}
