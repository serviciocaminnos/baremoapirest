<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Dispute_resolution;
use Illuminate\Http\Request;

class DisputeResolutionController extends Controller
{
    public function index()
    {
        $dispute_resolution = Dispute_resolution::all();
        if ($dispute_resolution != '[]'){
            return json_encode($dispute_resolution);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Solucion de controversias',
            ]);
        }
    }

    public function getDisputeResolution ($id_company){
        $dispute_resolution = Dispute_resolution::where('contact_id', 'like' , $id_company)->get();
        if ($dispute_resolution != '[]'){
            return json_encode($dispute_resolution);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen resolucion de controversias',
            ]);
        }
    }

    public function store(Request $request)
    {
        $Dispute_resolution = new Dispute_resolution();
        $Dispute_resolution->contact_id = $request->input ('contact_id');
        $Dispute_resolution->instance = $request->input ('instance');
        $Dispute_resolution->metod = $request->input ('metod');
        $Dispute_resolution->regulation = $request->input ('regulation');
        $Dispute_resolution->type = $request->input ('type');
        $Dispute_resolution->campus = $request->input ('campus');
        $Dispute_resolution->save();
        return response()->json([
            'status_code' => 200,
            'Dispute_resolution_id' => $Dispute_resolution->id,
        ]);
    }

    public function show($id)
    {
        $dispute_resolution = Dispute_resolution::find($id);
        if ($dispute_resolution != null){
            return json_encode($dispute_resolution);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Solucion de controversias',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $dispute_resolution = Dispute_resolution::find($id);
        if ($dispute_resolution != null){
            $dispute_resolution->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Solucion de controversias actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Solucion de controversias no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $dispute_resolution = Dispute_resolution::find($id);
        if ($dispute_resolution != null){
            $dispute_resolution->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Solucion de controvercias eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Solucion de controvercias no encontrado",
            ]);
        }
    }
}
