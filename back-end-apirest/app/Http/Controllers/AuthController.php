<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register (Request $request){
        $response = null;
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'position' => 'required',
            'rol' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails()){
            $response = response()->json([
                'status_code'=>400,
                'message'=> $validator->errors()->first()
            ]);
        }
        else {
            $user = new User();
            $user->company_id = $request->input ('company_id');
            $user->name = $request->input ('name');
            $user->lastname = $request->input ('lastname');
            $user->username = $request->input ('username');
            $user->position = $request->input ('position');
            $user->rol = $request->input ('rol');
            $user->email = $request->input ('email');
            $user->user_image = $request->input ('user_image');
            $user->password = Hash::make($request->input ('password'));
            $user->save();
            $response = response()->json([
                'status_code' => 200,
                'message' => 'user created successfully',
                'id_user' => $user->id,
            ]);
        }
        return $response;
    }

    public function login (Request $request){
        $validator = Validator::make($request->all(),[
            'email_or_username' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status_code' => 400,
                'message' => 'Error validating user data'
            ]);
        }

        $email_or_username = $request->input('email_or_username');
        $password = $request->input('password');
        if(filter_var($email_or_username, FILTER_VALIDATE_EMAIL)) 
        {     
            if (Auth::attempt(['email'=>$email_or_username,'password'=>$password]))
            {
                $user = User::where('email', '=', $request->input('email_or_username'))->first();
                $token = $user->createToken('authToken')->plainTextToken;
    
                return response()->json([
                    'status_code' => 200,
                    'token' => $token,
                    'idUsuario' => $user->id,
                    'rol' => $user->rol,
                    'name' => $user->name,
                    'lastname' => $user->lastname,
                    'username' => $user->username,
                    'position' => $user->position,
                    'email' => $user->email,
                ]);
            }
            else {
                return response()->json([
                    'status_code' => 500,
                    'message' => 'email o contraseña erroneas'
                ]);
            }
    
            
        } 
        else 
        {
            if (Auth::attempt(['username'=>$email_or_username,'password'=>$password]))
            {
                $user = User::where('username', '=', $request->input('email_or_username'))->first();
                $token = $user->createToken('authToken')->plainTextToken;
    
                return response()->json([
                    'status_code' => 200,
                    'token' => $token,
                    'idUsuario' => $user->id,
                    'rol' => $user->rol,
                    'name' => $user->name,
                    'lastname' => $user->lastname,
                    'username' => $user->username,
                    'position' => $user->position,
                    'email' => $user->email,
                ]);
            }
            else {
                return response()->json([
                    'status_code' => 500,
                    'message' => 'Nombre de usuario o contraseña erroneas'
                ]);
            }  
        }      
    }

    public function logout (Request $request){

        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'status_code' => 200,
            'message' => 'Token delete successfull',
        ]); 
    }
}
