<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Legal_reserve;
use Illuminate\Http\Request;

class LegalReserveController extends Controller
{
    public function index()
    {
        $legal_reserve = Legal_reserve::all();
        if ($legal_reserve != '[]'){
            return json_encode($legal_reserve);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existen Reserva legal',
            ]);
        }
    }

    public function store(Request $request)
    {
        $legal_reserve = new Legal_reserve();
        $legal_reserve->participation_id = $request->input ('participation_id');
        $legal_reserve->legal_reserve_fund = $request->input ('legal_reserve_fund');
        $legal_reserve->annual_percentage_retained = $request->input ('annual_percentage_retained');
        $legal_reserve->save();
        return response()->json([
            'status_code' => 200,
            'legal_reserve_id' => $legal_reserve->id,
        ]);
    }

    public function show($id)
    {
        $legal_reserve = Legal_reserve::find($id);
        if ($legal_reserve != null){
            return json_encode($legal_reserve);
        }
        else {
            return response()->json([
                'status_code' => 400,
                'message' => 'No existe Reserva legal',
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $legal_reserve = Legal_reserve::find($id);
        if ($legal_reserve != null){
            $legal_reserve->update($request->all());
            return response()->json([
                'status_code' => 200,
                'message' => "Reserva legal actualizada correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'message' => "Reserva legal no encontrado",
            ]);
        }
    }

    public function destroy( $id)
    {
        $legal_reserve = Legal_reserve::find($id);
        if ($legal_reserve != null){
            $legal_reserve->delete();
            return response()->json([
                'status_code' => 200,
                'mensaje' => "Reserva legal eliminado correctamente",
            ]);
        }
        else {
            return response()->json([
                'status_code' => 500,
                'mensage' => "Reserva legal no encontrado",
            ]);
        }
    }
}
