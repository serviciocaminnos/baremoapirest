<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Global_answer_sub_question extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_sub_question_evaluations',
        'id_sub_question',
        'name',
        'id_type_answer',
        'id_answer',
        'porcentage',
        'sub_answer',
        'need',
    ];
}
