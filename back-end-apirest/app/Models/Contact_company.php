<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact_company extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kind_of_society',
        'business_name',
        'trade_registration',
        'nit',
        'type_of_item',
        'date',
        'legal_address',
        'direction',
        'contact_person',
        'contact_number',
        'contact_email',
        'url_logo',
    ];
}
