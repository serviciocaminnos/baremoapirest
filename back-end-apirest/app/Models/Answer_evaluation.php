<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer_evaluation extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_question_evaluation',
        'id_subquestion_evaluation',
        'id_answer_type',
        'id_answer',
        'porcentage',
    ];
}
