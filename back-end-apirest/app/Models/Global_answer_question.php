<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Global_answer_question extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_question_evaluations',
        'id_question',
        'name',
        'id_type_answer',
        'id_answer',
        'porcentage',
        'sub_answer',
        'need',
    ];
}
